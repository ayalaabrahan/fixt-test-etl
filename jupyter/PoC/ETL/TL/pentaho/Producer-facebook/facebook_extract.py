from pymongo import MongoClient
from kafka import KafkaProducer
from time import sleep
from datetime import datetime
from abc import ABCMeta, abstractstaticmethod
import credentials
import defines
import json
import facebook
import schedule
import uuid


class IFacebookExtract(metaclass=ABCMeta):

        @abstractstaticmethod
        def print_name():
                """ implement in child class """


class FacebookExtract(IFacebookExtract):

        __instance = None

        def __init__(self, name):
                if FacebookExtract.__instance is not None:
                        raise Exception("Singleton cannot be instanciated more than once!")
                else:
                        FacebookExtract.name = name
                        FacebookExtract.__instance = self
                        self.initialize_deamon()

        @staticmethod
        def get_instance():
                if FacebookExtract.__instance is None:
                        FacebookExtract("Facebook")
                return FacebookExtract.__instance

        def initialize_deamon(self):
                        self.set_conections()
                        self.set_graph_api()
                        self.get_facebook_page_posts()
                        self.get_posts_comments()
                        self.get_comments_answers()

        def set_kafka_conection(self):
                kafka_host = defines.KAFKA_HOST
                kafka_port = defines.KAFKA_PORT
                encoding = defines.ENCODE
                self.kafka_producer = KafkaProducer(
                                                bootstrap_servers=[kafka_host+":"+kafka_port], api_version=(0, 11, 5),
                                                value_serializer=lambda x: json.dumps(x).encode(encoding))

        def set_mongo_collections(self):
                mongo_collections_names = defines.MONGO_COLLECTIONS
                """ for collection in mongo_collections_names:
                        self.mongo_collections.append(self.db[collection]) """

        def set_mongo_conection(self):
                mongo_host = defines.MONGO_HOST
                mongo_port = defines.MONGO_PORT
                mongo_db_name = defines.MONGO_DATABASE_NAME
                self.client = MongoClient(mongo_host+":"+mongo_port)
                self.db = self.client[mongo_db_name]
                self.set_mongo_collections()

        def set_conections(self):
                self.set_kafka_conection()
                self.set_mongo_conection()

        def set_graph_api(self):
                acces_token = credentials.TOKEN
                self.graph = facebook.GraphAPI(acces_token)

        def get_facebook_page_posts(self):
                print('extract post started\n')
                facebook_page_id = credentials.PAGE
                *feed, = self.graph.get_all_connections(id=facebook_page_id, connection_name='feed')
                self.feed = feed
                self.feed_raw = json.dumps(feed, ensure_ascii=False).encode('utf-8')
                print('extract post finished\n')

        def get_posts_comments(self):
                print('extract posts_comments started\n')
                self.posts_ids = [ current_post['id'] for current_post in self.feed ]
                composite_list = [self.posts_ids[i:i+50] for i in range(0, len(self.posts_ids),50)]
                comments_by_post = {}
                for each_list_ids in composite_list:        
                        current_comments = self.graph.get_objects(ids=each_list_ids, fields='id,comments')
                        comments_by_post.update(current_comments)
                self.comments_by_post = comments_by_post                
                print('extract posts_comments finished\n')

        def get_comments_answers(self):
                print('extract posts_comments_answers started\n')
                posts_comments = self.comments_by_post
                for key in posts_comments.keys():
                        if 'comments' in posts_comments[key]:
                                current_post_comments = posts_comments[key]['comments']['data']
                                for current_comment in current_post_comments:
                                        try:
                                                replies = self.graph.get_object(id=current_comment['id'], fields='comments')
                                                comentario = {
                                                        "Post ID": posts_comments[key]['id'],
                                                        "Comment":{ 
                                                                "Text": current_comment,
                                                                "Replies": replies
                                                        }         
                                                }
                                        except:
                                                print('error ' + current_comment['id'])
                                                continue
                print('extract posts_comments_answers finished\n')

        def get_one_object(self):
                obj_id = '100891253347846_3662633853840217'
                fld = 'id,message,created_time'
                obj = self.graph.get_object(id=obj_id, fields='id,message,created_time')
                obj2 = json.dumps(obj, ensure_ascii=False, indent=4).encode('utf-8')
                print(obj.decode())

        @staticmethod
        def print_name():
                print(f"Name: {FacebookExtract.__instance.name}")


def main():
        facebook_instance = FacebookExtract.get_instance()
        facebook_instance.print_name()


""" Primera ejecución """
main()

""" Ejecución programada """
schedule.every(1).minutes.do(main)

while 1:
        schedule.run_pending()
        sleep(1)
