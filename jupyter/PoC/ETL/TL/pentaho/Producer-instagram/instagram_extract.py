# To solve encoding error when printing post conetnt: change console encoder: chcp 65001 -> set PYTHONIOENCODING=utf-8
import defines
from datetime import datetime, timedelta
from pymongo import MongoClient
from kafka import KafkaProducer
from time import sleep
import pymongo
import re
import json
import time
import instaloader
import requests
import schedule
import uuid

# KAFKA CONNECTION
producer = KafkaProducer(bootstrap_servers=['localhost:9092'],
                    api_version=(0,11,5),
                    value_serializer=lambda x:
                    json.dumps(x).encode('utf-8'))

def extract():

    isVideo = False
    
    # Initial exec time (Instaloader)
    start = time.time()
    
    # Create an instance
    L = instaloader.Instaloader()
    L.login(defines.LOGIN_USER,defines.LOGIN_PASS)

    profile = instaloader.Profile.from_username( L.context, defines.ACC)

    print("--------ACCOUNT DATA----------")
    print('IGTV:', profile.igtvcount)
    print('Business account:', profile.is_business_account)
    print('External url:', profile.external_url)
    print('Media count:', profile.mediacount)
    print('Is private:', profile.is_private)
    print('Fullname:', profile.full_name)
    print('Following', profile.followees, " accounts")
    print('Followed by', profile.followers, " accounts")
    print('Bio:', profile.biography)
    
    print("------------POSTS-------------")
    profile_posts = profile.get_posts()

    img_id = 0
    all_posts = []
    lista_ayer = []
    
    for post in profile_posts:
        
        if post.mediaid not in lista_ayer:

            end = time.time()
            if post.video_duration is not None: isVideo = True
            
            post_id = str(uuid.uuid4())

            all_comments = []

            if post.comments > 0:

                for comment in post.get_comments():
                    
                    replies = []
                    
                    for answer in comment.answers:
                        replies.append(str(answer))
                    #endfor

                    #Create a dict per comment                    
                    comentario = {
                    "Post ID": post_id,
                    "Comment": {
                        "Text": str(comment),
                        "Replies": replies
                        }         
                    }
                    all_comments.append(comentario)
                #endfor      
            #endif
            
            imageList = []
            cont_img = 0
            cont_video = 0

            if post.mediacount > 1:
                for slide in post.get_sidecar_nodes():
                    if not slide.is_video:
                        cont_img += 1
                        imageList.append(slide.display_url)
                    else:
                        cont_video += 1
            else:
                imageList.append(post.url)
                if isVideo:
                    cont_video += 1
                else:
                    cont_img += 1
            

            #creating 1 dic per post
            post_dic = {
                "id": post.mediaid,
                "Profile": str(profile),
                "Local date": str(post.date_local).replace(",",":"),
                "Number of media files": post.mediacount,
                "Likes": post.likes,
                "Caption": post.caption,
                "Hashtags used": post.caption_hashtags,
                "Tagged users": post.tagged_users,
                "Image links": imageList,
                "Video duration": post.video_duration,
                "Video views": post.video_view_count,
                "Number of comments": len(all_comments),
            }

            istext = 0
            if post.caption is not None:
                istext = 1

            queryResult = {
            "uuid" : post_id,
            "status" : "raw",
            "query_date": str(datetime.now().strftime('%Y-%m-%d %H:%M:%S')),
            "last_status_date": str(datetime.now().strftime('%Y-%m-%d %H:%M:%S')),
            "social_network": "Instagram",
            "account": {
                "query_value": defines.ACC,
                "query_description": profile.biography
            },
            "media_type": {
                "text": istext,
                "image": cont_img,
                "video": cont_video
            },
            "raw_data": post_dic
            }
            #Sending the post trough the topic
            producer.send('instagramtest', value=str(queryResult))
            
            print(json.dumps(queryResult, indent=4, default=str))
        sleep(10)
    #endfor        
# enddef

extract()

# Running as daemon
schedule.every(24).hours.do(extract)

while 1:
    schedule.run_pending()
    time.sleep(1)
