from defines import getCreds, makeApiCall

def getAccountInfo( params ) :
	""" Get info on a users account
	
	API Endpoint:
		https://graph.facebook.com/{graph-api-version}/{ig-user-id}?fields=business_discovery.username({ig-username}){username,website,name,ig_id,id,profile_picture_url,biography,follows_count,followers_count,media_count}&access_token={access-token}
	Returns:
		object: data from the endpoint
	"""

	endpointParams = dict() # parameter to send to the endpoint
	endpointParams['fields'] = 'business_discovery.username(' + params['ig_username'] + '){username,website,name,ig_id,id,profile_picture_url,biography,follows_count,followers_count,media_count}' # string of fields to get back with the request for the account
	endpointParams['access_token'] = params['access_token'] # access token

	url = params['endpoint_base'] + params['instagram_account_id'] # endpoint url

	return makeApiCall( url, endpointParams, params['debug'] ) # make the api call

params = getCreds() # get creds
params['debug'] = 'no' # set debug
response = getAccountInfo( params ) # hit the api for some data!

print ("\n---- ACCOUNT INFO -----\n") # display latest post info
print ("username:") # label
print (response['json_data']['business_discovery']['username']) # display username
print ("\nwebsite:") # label
print (response['json_data']['business_discovery']['website']) # display users website
print ("\nnumber of posts:") # label
print (response['json_data']['business_discovery']['media_count']) # display number of posts user has made
print ("\nfollowers:") # label
print (response['json_data']['business_discovery']['followers_count']) # display number of followers the user has
print ("\nfollowing:") # label
print (response['json_data']['business_discovery']['follows_count']) # display number of people the user follows
print ("\nprofile picture url:") # label
print (response['json_data']['business_discovery']['profile_picture_url']) # display profile picutre url
#print ("\nbiography:") # label
#print (response['json_data']['business_discovery']['biography']) # display users about section

#---------------OUTPUT-------------
'''

---- ACCOUNT INFO -----

username:
semillas.de.la.ciencia

website:
http://semillasdelaciencia.com/

number of posts:
7

followers:
53

following:
1

profile picture url:
https://scontent-bog1-1.xx.fbcdn.net/v/t51.2885-15/69600720_743851652712144_833508390280888320_n.jpg?_nc_cat=104&ccb=1-3&_nc_sid=86c713&_nc_eui2=AeGZJ6C9zhnJrpS5fK8fwHlrm2AaqdYjGVibYBqp1iMZWE6A1nU_HtHiAghFytBZhlDhVOUaUOPFNebFkv27NyZn&_nc_ohc=Nyzjf1KDkzUAX_jhc0K&_nc_ht=scontent-bog1-1.xx&oh=ee75c53264d78b2f7c36463f5b1580eb&oe=60E49244
'''