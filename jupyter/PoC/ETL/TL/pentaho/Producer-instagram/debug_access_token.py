from defines import getCreds, makeApiCall
import datetime

def debugAccessToken( params ) :
	""" Get info on an access token 
	
	API Endpoint:
		https://graph.facebook.com/debug_token?input_token={input-token}&access_token={valid-access-token}
	Returns:
		object: data from the endpoint
	"""

	endpointParams = dict() # parameter to send to the endpoint
	endpointParams['input_token'] = params['access_token'] # input token is the access token
	endpointParams['access_token'] = params['access_token'] # access token to get debug info on

	url = params['graph_domain'] + '/debug_token' # endpoint url

	return makeApiCall( url, endpointParams, params['debug'] ) # make the api call

params = getCreds() # get creds
params['debug'] = 'yes' # set debug
response = debugAccessToken( params ) # hit the api for some data!

print ("\nData Access Expires at: ") # label
print (datetime.datetime.fromtimestamp( response['json_data']['data']['data_access_expires_at'] )) # display out when the token expires

print ("\nToken Expires at: ") # label
print (datetime.datetime.fromtimestamp( response['json_data']['data']['expires_at'] )) # display out when the token expires

#-------------OUTPUT-------------

'''

URL: 
https://graph.facebook.com//debug_token

Endpoint Params: 
{
    "input_token": "EAAHqjVaYMvQBACQiZB92ILTeeJfy0CY8LCjCns4P5hVcH2e80Yj6e2gQaQdids8GdSI0eJS8c98tZC2lZAubdKO8obFeRWipOc8wYYA1s38pOKD1AoVFShggrRuj9OBcux5QPy3BcJ6mVNZBvGZBx7AeWGVD5oOVNgycpdMMsSqZBOOjRh8QdFf21qGM6ePZB0ZD",
    "access_token": "EAAHqjVaYMvQBACQiZB92ILTeeJfy0CY8LCjCns4P5hVcH2e80Yj6e2gQaQdids8GdSI0eJS8c98tZC2lZAubdKO8obFeRWipOc8wYYA1s38pOKD1AoVFShggrRuj9OBcux5QPy3BcJ6mVNZBvGZBx7AeWGVD5oOVNgycpdMMsSqZBOOjRh8QdFf21qGM6ePZB0ZD"
}

Response: 
{
    "data": {
        "app_id": "539367740814068",
        "type": "USER",
        "application": "Graph_API_test",
        "data_access_expires_at": 1632973228,
        "expires_at": 0,
        "is_valid": true,
        "issued_at": 1625246052,
        "scopes": [
            "read_insights",
            "pages_show_list",
            "instagram_basic",
            "instagram_manage_comments",
            "instagram_manage_insights",
            "instagram_content_publish",
            "instagram_manage_messages",
            "pages_read_engagement",
            "public_profile"
        ],
        "granular_scopes": [
            {
                "scope": "pages_show_list"
            },
            {
                "scope": "instagram_basic"
            },
            {
                "scope": "instagram_manage_comments"
            },
            {
                "scope": "instagram_manage_insights"
            },
            {
                "scope": "instagram_content_publish"
            },
            {
                "scope": "instagram_manage_messages"
            },
            {
                "scope": "pages_read_engagement"
            }
        ],
        "user_id": "4467022209977455"
    }
}

Data Access Expires at: 
2021-09-29 22:40:28

Token Expires at: 
1969-12-31 19:00:00

'''