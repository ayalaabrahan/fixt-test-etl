from defines import getCreds, makeApiCall

def getLongLivedAccessToken( params ) :
	""" Get long lived access token
	
	API Endpoint:
		https://graph.facebook.com/{graph-api-version}/oauth/access_token?grant_type=fb_exchange_token&client_id={app-id}&client_secret={app-secret}&fb_exchange_token={your-access-token}
	Returns:
		object: data from the endpoint
	"""

	endpointParams = dict() # parameter to send to the endpoint
	endpointParams['grant_type'] = 'fb_exchange_token' # tell facebook we want to exchange token
	endpointParams['client_id'] = params['client_id'] # client id from facebook app
	endpointParams['client_secret'] = params['client_secret'] # client secret from facebook app
	endpointParams['fb_exchange_token'] = params['access_token'] # access token to get exchange for a long lived token

	url = params['endpoint_base'] + 'oauth/access_token' # endpoint url

	return makeApiCall( url, endpointParams, params['debug'] ) # make the api call

params = getCreds() # get creds
params['debug'] = 'yes' # set debug
response = getLongLivedAccessToken( params ) # hit the api for some data!

print ("\n ---- ACCESS TOKEN INFO ----\n") # section header
print ("Access Token:")  # label
print (response['json_data']['access_token']) # display access token


#-------------OUTPUT---------------------
'''

URL: 
https://graph.facebook.com/v11.0/oauth/access_token

Endpoint Params: 
{
    "grant_type": "fb_exchange_token",
    "client_id": "539367740814068",
    "client_secret": "c7167ff82a0f64f8a5e0ed0164760a97",
    "fb_exchange_token": "EAAHqjVaYMvQBACQiZB92ILTeeJfy0CY8LCjCns4P5hVcH2e80Yj6e2gQaQdids8GdSI0eJS8c98tZC2lZAubdKO8obFeRWipOc8wYYA1s38pOKD1AoVFShggrRuj9OBcux5QPy3BcJ6mVNZBvGZBx7AeWGVD5oOVNgycpdMMsSqZBOOjRh8QdFf21qGM6ePZB0ZD"
}

Response: 
{
    "access_token": "EAAHqjVaYMvQBAO95WbBF06rNzghVIHOhJeGG4yqCoWYF0T2LwZAkLylDcrM8rO68TomIdOfXr7vcgOmVMexD6AtZB2XiMkPcl7dM96v9iHB9Ka4VdueSweWp2UokIYnfFaI04AMkjNh7eXq2ZBkZCeYlrp0IAqSC5bQnOH4RHo0oxfUaaj8Bvpf2wa8DMXIZD",
    "token_type": "bearer"
}

 ---- ACCESS TOKEN INFO ----

Access Token:
EAAHqjVaYMvQBAO95WbBF06rNzghVIHOhJeGG4yqCoWYF0T2LwZAkLylDcrM8rO68TomIdOfXr7vcgOmVMexD6AtZB2XiMkPcl7dM96v9iHB9Ka4VdueSweWp2UokIYnfFaI04AMkjNh7eXq2ZBkZCeYlrp0IAqSC5bQnOH4RHo0oxfUaaj8Bvpf2wa8DMXIZD


'''