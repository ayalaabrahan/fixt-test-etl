from googleapiclient.discovery import build
import json
from pymongo import MongoClient
from time import sleep
from json import dumps
from pymongo import MongoClient
from json import loads
from urllib.parse import urlparse, parse_qs
from kafka import KafkaProducer
from datetime import date
from datetime import datetime
import defines
import credentials
import schedule
import uuid
import time
import requests
import urllib.request
import urllib.parse as p
import re

# Main function to extract video data:
comments = []
youtube = build('youtube', 'v3', developerKey=credentials.API_KEY)
def job():
    
    call(defines.ACCOUNT)
    


def video_comments(video_id):
    date_before = datetime.now()

    # empty list for storing reply
    replies = []

    # retrieve youtube video results
    video_response = youtube.commentThreads().list(
        part='snippet,replies',
        videoId=video_id
    ).execute()

    # iterate video response
    while video_response:

        # extracting required info
        # from each result object
        for item in video_response['items']:

            # Extracting comments
            comment = item['snippet']['topLevelComment']['snippet']['textDisplay']

            # counting number of reply of comment
            replycount = item['snippet']['totalReplyCount']

            # if reply is there
            if replycount > 0:

                # iterate through all reply
                for reply in item['replies']['comments']:

                    # Extract reply
                    reply = reply['snippet']['textDisplay']

                    # Store reply is list
                    replies.append(reply)

            # print comment with list of reply
            print(comment, replies, end='\n\n')
            comments.append(comment)
            comments.append(replies)
            print('-------------------')
            print(comments)

            # empty reply list
            replies = []

        # Again repeat
        if 'nextPageToken' in video_response:
            video_response = youtube.commentThreads().list(
                part='snippet,replies',
                videoId=video_id
            ).execute()
        else:
            break
    return comments
    #producer.send('youtubetest', value=dict)
    # collection.insert_one(dict)
    #print('{} added to {}'.format(dict, collection))


def extract_video_id(url):

    query = urlparse(url)
    if query.hostname == 'youtu.be':
        return query.path[1:]
    if query.hostname in {'www.youtube.com', 'youtube.com'}:
        if query.path == '/watch':
            return parse_qs(query.query)['v'][0]
        if query.path[:7] == '/watch/':
            return query.path.split('/')[1]
        if query.path[:7] == '/embed/':
            return query.path.split('/')[2]
        if query.path[:3] == '/v/':
            return query.path.split('/')[2]
        # below is optional for playlists
        if query.path[:9] == '/playlist':
            return parse_qs(query.query)['list'][0]
   # returns None for invalid YouTube url


def getVideoImage(video_id):
    pic_url = 'https://img.youtube.com/vi/'+video_id+'/0.jpg'
    FileName =  "./YT_photos/" +video_id+'.jpg'

    with open(FileName, 'wb') as handle:
        response = requests.get(pic_url, stream=True)

        if not response.ok:
            print(response)

        for block in response.iter_content(1024):
            if not block:
                break
            handle.write(block)


def getChannelID(Channel_name):

    #youtube = build('youtube', 'v3', developerKey=api_key)

    search_channel_name = Channel_name
    channels_response = youtube.channels().list(
        forUsername=search_channel_name,
        part="id"
    ).execute()
    information = channels_response['items']
    aux = information[0]

    return str(aux['id'])


def getChannelName(ChannelID):

    search_channel_name = ChannelID
    channels_response = youtube.channels().list(
        id=search_channel_name,
        part="snippet"
    ).execute()
    information = channels_response['items']
    aux = information[0]
    snippet = aux['snippet']
    return str(snippet['title'])


def getChannelInfo(Channel_id):
    #youtube = build('youtube', 'v3', developerKey=api_key)

    search_channel_id = Channel_id
    channels_response = youtube.channels().list(
        id=search_channel_id,
        part="id, statistics"
    ).execute()
    #print('INFORMATION: '+str(channels_response))
    information = channels_response['items']
    aux = information[0]
    return aux


def get_all_video_in_channel(channel_id):

    base_video_url = 'https://www.youtube.com/watch?v='
    base_search_url = 'https://www.googleapis.com/youtube/v3/search?'

    first_url = base_search_url + \
        'key={}&channelId={}&part=snippet,id&order=date&maxResults=1000'.format(
            credentials.API_KEY, channel_id)

    video_links = []
    url = first_url
    while True:
        inp = urllib.request.urlopen(url)
        resp = json.load(inp)

        for i in resp['items']:
            if i['id']['kind'] == "youtube#video":
                video_links.append(base_video_url + i['id']['videoId'])

        try:
            next_page_token = resp['nextPageToken']
            url = first_url + '&pageToken={}'.format(next_page_token)
        except:
            break
    return video_links


def extract_channel_ID(url):

    query = urlparse(url)
   # if query.hostname == 'youtu.be': return query.path[1:]
    if query.hostname in {'www.youtube.com', 'youtube.com'}:

        if query.path[:6] == '/user/':
            return getChannelID(query.path.split('/')[2])

        if query.path[:3] == '/c/':
            return getChannelID(query.path.split('/')[2])

        if query.path[:9] == '/channel/':
            return query.path.split('/')[2]

   # returns None for invalid YouTube url


def get_video_details(**kwargs):
    return youtube.videos().list(
        part="snippet,contentDetails,statistics",
        **kwargs
    ).execute()


def print_video_infos(video_response):
    items = video_response.get("items")[0]
    # get the snippet, statistics & content details from the video response
    snippet = items["snippet"]
    statistics = items["statistics"]
    content_details = items["contentDetails"]
    # get infos from the snippet
    channel_title = snippet["channelTitle"]
    title = snippet["title"]
    description = snippet["description"]
    publish_time = snippet["publishedAt"]
    # get stats infos
    comment_count = statistics["commentCount"]
    like_count = statistics["likeCount"]
    dislike_count = statistics["dislikeCount"]
    view_count = statistics["viewCount"]
    duration = content_details["duration"]

    print(f"""\
    Title: {title}
    Description: {description}
    Channel Title: {channel_title}
    Publish time: {publish_time}
    Duration: {duration}
    Number of comments: {comment_count}
    Number of likes: {like_count}
    Number of dislikes: {dislike_count}
    Number of views: {view_count}
    """)


# --------------KAFKA & MONGO CONNECTION---------------------
producer = KafkaProducer(bootstrap_servers=['localhost:9092'],
    api_version=(0,11,5),
    value_serializer=lambda x:
    dumps(x).encode('utf-8'))


# Call function
def call(URL):
    # Initial exec time (Instaloader)
    start = time.time()

    ChannelID = extract_channel_ID(URL)
    print('ID FINAL: '+ChannelID)
    ChannelName = getChannelName(ChannelID)
    print('Nombre: '+ChannelName)
    ChannelInfo = getChannelInfo(ChannelID)
    print('Information: '+str(ChannelInfo))
    videosList = get_all_video_in_channel(ChannelID)
    print("Tamano" + str(len(videosList)))
    print(videosList)
    post_id = str(uuid.uuid4())
    all_comments = []

    #Videos extraction and load
    for id in videosList:

        video_id = extract_video_id(id)
        print(video_id)
        #getVideoImage(video_id)
        Video_info = get_video_details(id=video_id)
        print_video_infos(Video_info)

        items = Video_info.get("items")[0]
        statistics = items["statistics"]
        comment_Count = statistics["commentCount"]
        comments = []
        
        #Comments extraction and load
        if(int(comment_Count) > 0):
            comments = video_comments(video_id)
            comentario = {
                    "Post ID": post_id,
                    "Comment": {
                        "comments": comments
                        }         
                    }
            all_comments.append(comentario)
            #YT_comments_collection.insert_one(comentario)


        print('\n')
        
        end = time.time()

        Video = {
            'ID' : video_id,
            'Information' : Video_info
        }

        sleep(10)
        dict = {    'uuid': post_id,
                        'status': 'raw',
                        'query_date': str(datetime.now().strftime('%Y-%m-%d %H:%M')),
                        'last_status_date' : str(datetime.now().strftime('%Y-%m-%d %H:%M')),
                        'social_network': "YouTube",
                        'account':{ "query_value": defines.ACCOUNT,
                                    "query_description": ChannelName},
                        "media_type": {"text" :1,
                                    "photo":1,
                                    "video":1},
                        "response_time": end-start,
                        'raw_data': Video
                        }      
        print(dict) 
       
        producer.send('youtube', value=str(dict))  
        #videos_collection.insert_one(dict)


    

# Primera ejecución
job()
# Ejecución programada

schedule.every(80).minutes.do(job)

while 1:
    schedule.run_pending()
    sleep(1)
