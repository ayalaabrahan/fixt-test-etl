# ========================================= #
## ========= Importando librerias ======== ##
# ========================================= #
import credentials
import defines
import schedule
from pymongo import MongoClient
from kafka import KafkaProducer
import tweepy
from time import sleep
#from pymongo import MongoClient
from datetime import datetime
import json
import requests
import uuid

#TweetListener class
class TweetsListener(tweepy.StreamListener):
    def on_connect(self):
        print("Estoy conectado!")

    def on_status(self, status):
        tweet = status._json
        #print(status._json)
        #print("============================================\n==========================\n==============================")
        #print(tweet)
        post_id = str(uuid.uuid4())
        cont_media_photo = 0
        cont_media_video = 0
        #date_before = datetime.now()
        #date_after = datetime.now()
        if not status.text.startswith("RT"):
            if 'extended_entities' in tweet:
                print("a")            
                for media in tweet['extended_entities']['media']:
                    print(tweet['extended_entities']['media'])
                    if media["type"] == 'photo':
                        url = media["media_url"]
                        url_img = url.split("/")
                        url_img = url_img[-1]
                        print(url_img)
                        path = "./tweets_photos/" + url_img
                        with open(path, 'wb') as handle:
                            response = requests.get(url, stream=True)
                            if not response.ok:
                                print (response)
                            for block in response.iter_content(1024):
                                if not block:
                                    break
                                handle.write(block)
                        cont_media_photo += 1
                    if media["type"] == 'video' or media["type"] == 'animated_gif':
                        print(media["media_url"])
                        cont_media_video += 1
            print(cont_media_photo)
            print(cont_media_video)

            dict = {    'uuid': post_id,
                        'status': 'raw',
                        'query_date': str(datetime.now()),
                        'last_status_date' : str(datetime.now()),
                        'social_network': "Twitter",
                        'account':{ "query_value’": defines.ACCOUNT["username"],
                                    "query_description": "Cuenta oficial de Every Three Minutes"},
                        "media_type": {"text" :1,
                                    "photo":cont_media_photo,
                                    "video":cont_media_video},
                        'response_time': 'NA',
                        'raw_data': tweet
                        }      
            print(dict) 
            producer.send('twittertest', value=str(dict))  
            collection.insert_one(dict)
        
        
        sleep(5)


    def on_error(self, status_code):
        print("Error", status_code)



#Main function to extract tweets
def job():

    # ========================================= #
    ## ======= Credenciales de Twitter ======= ##
    # ========================================= #
    auth = tweepy.OAuthHandler(credentials.API_KEY, credentials.API_SECRET_KEY)
    auth.set_access_token(credentials.ACCESS_TOKEN, credentials.ACCESS_TOKEN_SECRET)

    #api = tweepy.API(auth)
    api = tweepy.API(auth, wait_on_rate_limit_notify = True, wait_on_rate_limit = True)

    # =========================================== #
    ## ======= Conexiones Kafka y mongodb ======= ##
    # =========================================== #


    
    
    #Streaming
    stream = TweetsListener()
    streamingApi = tweepy.Stream(auth=api.auth, listener=stream)
    streamingApi.filter(
        follow=[defines.ACCOUNT["id"]],
        #track=["Trump"],
        #locations=[-74.1618785911,4.5526357576,-74.012418498,4.7579348352] # Ciudad de Mexico
    )
    #Location filter https://boundingbox.klokantech.com/
#endef




producer = KafkaProducer(bootstrap_servers=['kafka:9092'],
                        api_version=(0,11,5),
                        value_serializer=lambda x: 
                        json.dumps(x).encode('utf-8'))


client = MongoClient('mongo:27017')
db = client[defines.DATABASE_NAME]
collection = db[defines.COLLECTION_NAME]

#Primera ejecución
job()
#Ejecución programada
schedule.every(1).minutes.do(job)

while 1:
    schedule.run_pending()
    sleep(1)

