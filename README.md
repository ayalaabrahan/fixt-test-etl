# Etl Team

Este documento presenta la descripción del código creado como parte del trabajo de grado: _Módulo de ETL y visualización descriptiva de la ingesta de datos de redes sociales_, como solución para la etapa 1 del _módulo 0: captura de redes sociales_ del proyecto del CAM.

<br/>
---
Tambien, en los siguientes enlaces se encuentra material asociado a la entrega del proyecto:

[Carpeta Asociada asociada a Protocolo de Informacion de Equipo](https://livejaverianaedu.sharepoint.com/:f:/s/Caoba-CAM/EnEqG58P5jRLpp5xoM5p4AEB0PiOy6l25dlj8lIF16h96Q?e=vANPgH)

[Presentacion del proyecto final](https://www.canva.com/design/DAEwCyGLmp0/vBQHvvF5hUQDrxMXFxrR1w/view?utm_content=DAEwCyGLmp0&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton)

[Video Demo de Resultado](https://www.youtube.com/watch?v=Z5ce2LkR-Ro)

[Hacer copia y llenar plantilla de Comunicaciones](https://docs.google.com/document/d/1Lc-vkdru-VcBhAuCe9aKOgc99HhGIM9m/edit#heading=h.30j0zll)

[Borrador de documento de Grado (No se habilito)]

[Documento de requerimientos (No se habilito)]

<br/>
---
Tambien, en los siguientes enlaces se encuentran material asociado al entendimiento del proyecto luego de entrega y validaciones posteriores:

[Grabacion: Recontexto del proyecto con Fabian del Equipo de Cam-ETL](https://livejaverianaedu.sharepoint.com/:v:/s/Caoba-CAM/EVcWLtmqFBtKlLtIssf8jCYBLPmiUfeyA75-ZWY_zyErzg?e=S6ae00)

[Grabacion 2 (No se habilito)]


## Tabla de Contenidos
* [Descripción de la solución](#descripcion-solucion)
* [Requerimientos](#requerimientos)
* [Librerias Empleadas](#librerias-empleadas)
* [Requerimientos Hardware](#requerimientos-hardware)
* [Requerimientos Software](#requerimientos-software)
* [Arquitectura de la solución](#arquitectura-solucion)
* [Arquitectura de Componentes](#arquitectura-componentes)
* [Autores](#autores)

## Descripción de la solución

### Objetivo General: 
Desarrollar un módulo de software que extraiga, transforme y cargue datos de redes sociales, cuyo proceso pueda ser monitoreado a partir de visualizaciones descriptivas en el marco del proyecto Centro Activo de Monitoreo (CAM):

![](docs/readme/02-propuesta-solucion.png)

### Objetivo Específicos:
* Especificar los requisitos y el diseño del módulo de software.
* Recolectar datos de redes sociales, tales como Twitter, Facebook, Instagram y YouTube, generando un escenario de datos en bruto como insumo para la fase de transformación. 
* Procesar cada uno de los posts recolectados en la fase anterior de tal manera que sean transformados a datos canónicos de mayor granularidad. 
* Almacenar los datos transformados en una base de datos que pueda ser consumida por módulos posteriores del proyecto CAM. 
* Generar un dashboard de variables y datos almacenados en el proceso, de tal manera que den cuenta de manera individual y agregada acerca de la información del proceso de ingesta de datos de redes sociales. 

### Contexto del Problema

![](docs/readme/00-contexto1.png)

### Propuesta de Solucion

![](docs/readme/01-contexto2.png)

### Entregables

Los entregables asociados de acuerdo a los objetivos especificos fueron:

![](docs/readme/02-entregables.png)

### Preguntas de Investigación Esenciales:

1. Redes sociales como escenarios de generación masiva de información.
2. Interés de las organizaciones en enriquecer sus procesos y toma de decisiones a partir de dicha información.
3. Proceso de extracción, transformación y carga en función de datos provenientes de  cuatro redes sociales.

## Arquitectura de la solución

-- Las especificaciones funcionales son las siguientes:

![](docs/readme/03-especificacion-funcional.png)

-- Igualmente, la arquitectura de la solución alcanzada fue la siguiente:

![](docs/readme/04-arquitectura-altonivel.png)

### A nivel de persistencia en la arquitectura propuesta se genero:

![](docs/readme/05-persistencia.png)

### A nivel de los procesos de extracción en la arquitectura propuesta se genero:

![](docs/readme/06-proceso-extraccion.png)

![](docs/readme/07-proceso1-trans-load.png)

![](docs/readme/08-proceso2-trans-load.png)

## Estructura del proyecto

```
.
├── README.md
├── Dockerfile
├── data/
├── docs/
│   ├── Manual\ de\ usuario/
│   │   └── Manual\ De\ Usuario.pdf
│   ├── Manual\ técnico/
│   │   ├── Facebook/
│   │   │   ├── Documentación\ de\ funciones.docx
│   │   │   └── coverage/
│   │   ├── Instagram/
│   │   │   ├── Documentación\ de\ funciones.docx
│   │   │   └── coverage/
│   │   ├── Twitter/
│   │   │   ├── Documentación\ de\ funciones.docx
│   │   │   └── coverage/
│   │   └── YouTube/
│   │       ├── Coverage/
│   │       └── Documentación\ de\ funciones.docx
│   └── readme/
├── src/
│   └── logic/
│       ├── Kafka/
│       │   ├── dockerfile
│       │   ├── server.properties
│       │   └── start-kafka.sh
│       ├── Mongo/
│       │   └── docker-compose.yaml
│       ├── Pentaho/
│       │   ├── Dockerfile
│       │   ├── Transformation-Jobs/
│       │   │   ├── repositories/
│       │   │   │   ├── kafka-consumer/
│       │   │   │   └── transformaciones_pentaho.zip
│       │   │   └── repositories.xml
│       │   ├── requirements.txt
│       │   ├── start-pentaho.sh
│       │   └── steps/
│       │       ├── CPythonScriptExecutor/
│       │       │   └── version.xml
│       │       └── pentaho-cpython/
│       │           ├── lib/
│       │           └── pentaho-cpython-plugin-1.5.jar
│       ├── PowerBI/
│       │   ├── logos/
│       │   │   ├── 2630b2_b93a6b329fc44c73918132b8b9d5cac2_mv2.webp
│       │   │   ├── Instagram.png
│       │   │   ├── Javeriana.jpg
│       │   │   ├── caoba.png
│       │   │   ├── caoba_1.jpg
│       │   │   ├── facebook.png
│       │   │   ├── facebook.svg
│       │   │   ├── home.png
│       │   │   ├── twitter.png
│       │   │   └── youtube.png
│       │   ├── visualizacion_fase2-10sep2021.pbix
│       │   ├── visualizacion_fase3-6oct2021.pbix
│       │   └── visualizacion_fase4-propuesta-18oct2021.pbix
│       ├── Python/
│       │   ├── Instagram/
│       │   │   ├── Producer/
│       │   │   │   ├── __init__.py
│       │   │   │   ├── __pycache__/
│       │   │   │   ├── credentials.py
│       │   │   │   ├── defines.py
│       │   │   │   ├── desktop.ini
│       │   │   │   ├── dockerfile
│       │   │   │   ├── instagram_extract.py
│       │   │   │   ├── instaloaderExtract.py
│       │   │   │   ├── requirements.txt
│       │   │   │   └── start-extract-instagram.sh
│       │   │   └── __init__.py
│       │   ├── Twitter/
│       │   │   └── Producer/
│       │   │       ├── APIExtract.py
│       │   │       ├── credentials.py
│       │   │       ├── defines.py
│       │   │       ├── dockerfile
│       │   │       ├── extract_methods.py
│       │   │       ├── requirements.txt
│       │   │       ├── start-extract-twitter-export.sh
│       │   │       ├── start-extract-twitter.sh
│       │   │       ├── tweets_listener.py
│       │   │       └── twitter_extract.py
│       │   ├── Youtube/
│       │   │   └── Producer/
│       │   │       ├── 2017-04-07_19-02-10_UTC.jpg
│       │   │       ├── APIExtract.py
│       │   │       ├── credentials.py
│       │   │       ├── defines.py
│       │   │       ├── dockerfile
│       │   │       ├── requirements.txt
│       │   │       ├── start-extract-youtube.sh
│       │   │       └── youtube_extract.py
│       │   ├── connection/
│       │   │   ├── APIExtract/
│       │   │   │   └── IAPIExtract.py
│       │   │   ├── Extracter/
│       │   │   │   └── extracter.py
│       │   │   ├── kafka/
│       │   │   │   ├── kafka_svs.py
│       │   │   │   └── streamer.py
│       │   │   └── mongo/
│       │   │       ├── mongo_svs.py
│       │   │       └── storage.py
│       │   ├── export_mongo/
│       │   │   └── producer/
│       │   │       ├── __pycache__/
│       │   │       ├── defines.py
│       │   │       ├── dockerfile
│       │   │       ├── export_mongo_data.py
│       │   │       ├── requirements.txt
│       │   │       └── start-export-mongo.sh
│       │   ├── facebook/
│       │   │   └── producer/
│       │   │       ├── APIExtract.py
│       │   │       ├── credentials.py
│       │   │       ├── defines.py
│       │   │       ├── dockerfile
│       │   │       ├── facebook_extract.py
│       │   │       ├── facebook_extract_performance_threads.py
│       │   │       ├── requirements.txt
│       │   │       └── start-extract-facebook.sh
│       │   ├── test/
│       │   │   ├── ig_photos/
│       │   │   │   └── eltiempo/
│       │   │   ├── test_facebook_extract.py
│       │   │   ├── test_instagram_extract.py
│       │   │   ├── test_twitter_extract.py
│       │   │   └── test_youtube_extract.py
│       │   └── util/
│       │       ├── downloads/
│       │       │   ├── download.py
│       │       │   └── download_svs.py
│       │       └── logs/
│       │           ├── log.py
│       │           └── logs_svs.py
│       ├── Zookeeper/
│       │   ├── dockerfile
│       │   ├── start-zookeeper.sh
│       │   └── zookeeper.properties
│       └── docker-compose.yaml
├── jupyter/
│   └── PoC/
│       └── ETL/
│           └── TL/
│               └── pentaho/
│                   ├── PoC\ -\ ETL.ktr
│                   ├── Producer-facebook/
│                   ├── Producer-instagram/
│                   ├── Producer-twitter/
│                   ├── Producer-youtube/
│                   ├── Prueba_conexion.ipynb
│                   ├── correo.csv
│                   ├── correo.txt
│                   ├── correo_d.csv
│                   ├── get-ig.ktr
│                   ├── get-tw.ktr
│                   ├── get-yt-m.ktr
│                   ├── get-yt.ktr
│                   ├── job.kjb
│                   ├── process-ig.ktr
│                   ├── process-tw.ktr
│                   ├── process-yt-m.ktr
│                   ├── prueba.ktr
│                   ├── sample2.json
│                   ├── yt_video.json
│                   └── yt_video.txt
├── dashboard/
│   ├── desktop.ini
│   ├── logos/
│   ├── visualizacion_fase2-10sep2021.pbix
│   ├── visualizacion_fase3-6oct2021.pbix
│   ├── visualizacion_fase4-propuesta-18oct2021\ -\ copia.pbix
│   ├── visualizacion_fase5-final-11nov2021\ -\ copia.pbix
│   └── visualizacion_fase5-temporal.pbix
├── sonar-project.properties
├── htmlcov/
├── conf/
└── temp/
├── prueba.py
```
## Proceso de ejecucion y despliegue

Remitirse al adjunto ubicado en etl-team/docs/Manual de usuario/Manual De Usuario.pdf

### Visualización

Para generar un punto de partida para las visualizaciones, el tablero desarrollado se estructura a partir de un índice que ofrece una visualización descriptiva sobre los posts extraídos para las redes sociales de manera individual y agregada. Esto con el objetivo de tener un primer vistazo al resultado de la extracción de los datos.
Asimismo, dentro de la estaña de índice se tienen los botones de navegación a cada una de las visualizaciones individuales de las redes sociales en la mitad izquierda de la pantalla. Dentro de estas opciones el usuario puede elegir su red social de interés, así como las cuentas por las cuales desea filtrar las visualizaciones. Estas visualizacio-nes se verán a continuación.
Por último, en la esquina inferior derecha se tiene un botón de navegación relaciona-do a la información del proyecto desarrollado. Este botón redirige a una pestaña de descripción del proyecto y actores involucrados en el mismo.

![](docs/readme/09-visualizacion.png)

### Requerimientos Software

A continuación, se presentan los requerimientos correspondientes de software para el despliegue de la aplicación desarrollada.

| Requerimiento                    |            |
|----------------------------------|------------|
| Python                           |  >=3.8     |
| Sistemas de control de versiones | Git 1.8.3  |
| Docker                           | 20.10.8    |
| Docker-Compose                   | 3.8        |
| Java                             | 1.8        |
| Kafka                            | 3.0.0      |
| Zookeper                         | 3.0.0      |
| Pentaho                          | 9.2.0.0    |
| Mongo                            | 5          |
| Power BI                         |            |
| Power automate                   |            |
| One drive                        |            |


### Requerimientos Hardware

A continuación, se presentan los requerimientos correspondientes de hardware para el despliegue de la aplicación desarrollada.

| Requerimiento   |         |
|-----------------|---------|
| Memoria RAM     | 60GB    |
| Almacenamiento  | 1000 GB |
| CPU             | 32 vCore|

## Uso
En el siguiente video se aprecian las funcionalidades mas importantes del software desarrollado:
[youtube.com/watch?v=Z5ce2LkR-Ro]

## Soporte
Soporte a cargo del equipo del CAM.

## Roadmap
- Las restricciones actuales que presentan las API de uso gratuito suponen impedimentos en tiempos de espera para la extracción continua de los posts, así como en la cantidad de cuentas que permite dentro de la extracción. El tener menos restricciones en las API puede llevar a procesos más rápidos y a escalar con mayor facilidad el proyecto para un uso más extenso. Se sugiere evaluar la opcion de considerar las opciones pagas de las APIs de Twitter y YouTube.
- Eventualmente podria llegar a ser necesario cambiar las cuentas sobre las cuales se extraen datos de manera constante, para lo cual sería conveniente no tener que reiniciar los contenedores luego de realizar dicha modificacion. Permitir el cambio de cuentas sin tener que detener el funcionamiento de los conetnedores seria una mejora util.
- Desarrollo de un frontend para acceder a los servicios desarrollados.

Igualmente, como conclusiones relevantes del desarrollo del proyecto y su impacto puede referirse a lo siguiente:

![](docs/readme/10-conclusions.png)

## Licencias
Desarrollo exclusivo dentro del centro activo de monitoreo de Alianza CAOBA.

## Status del Proyecto
Dado que los requerimientos acordados inicialmente con el equipo del CAM han sido cumplidos, el desarrollo del proyecto está en pausa.

## Autores

| Organización          | Nombre del Miembro                      | Correos Asociados |
|-----------------------|-----------------------------------------|--------------------|
| PUJ-Bogota Alianza CAOBA  |  Líder Proyecto: Luis Gabriel Moreno Sandoval  | morenoluis@javeriana.edu.co |
| PUJ-Bogota Alianza CAOBA  |  Líder Proyecto: Maria Camila Martinez  | cientificodatos1@alianzacaoba.co |
| Universidad Javeriana | Alex David Barreto Alfonso              | @alexdb200011      |
| Universidad Javeriana | Fabián Alexis Pallares Jaimes           | @FabianPallaresj   |
| Universidad Javeriana | Sebastián Roberts Serrato               | @srob01            |
| Universidad Javeriana | Joan Sebastián Velandia Guataquira      | @JoanVelandia      |

