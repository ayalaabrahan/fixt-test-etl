MONGO_HOST = 'mongo'
MONGO_PORT = '27017'

MONGO_DBS = [{'db': 'Twitter',  'collection': 'twitter_agg'},
             {'db': 'Instagram', 'collection': 'instagram_agg'},
             {'db': 'Facebook', 'collection': 'facebook_agg'},
             {'db': 'Youtube', 'collection': 'youtube_agg'}]

