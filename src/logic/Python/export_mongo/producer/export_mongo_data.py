import defines
from connection.mongo.mongo_svs import StorageSvs
import pandas as pd
import schedule
import os
from time import sleep
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from datetime import datetime

def set_mongo_conection():
    try:
        mongo_host = defines.MONGO_HOST
        mongo_port = defines.MONGO_PORT
        StorageSvs.create_client(mongo_host, mongo_port)
    except Exception as e:
        print(e)

def get_dataframe(db, collection_name):
    mongo_cursor = StorageSvs.get_cursor(db, collection_name)
    df =  pd.DataFrame(list(mongo_cursor))
    if '_id' in df:
        del df['_id']
    return df

def create_csv(df, db_name, collection_name):
    path = "./" + db_name
    filename = collection_name + ".csv" 
    if not os.path.exists(path):
        os.mkdir(path)
    
    store_path = path + "/" + filename
    df.to_csv(store_path, index = False)
    return store_path

def send_attachments(file_list):
    now = datetime.now().strftime("%Y-%m-%d")
    #now = datetime.strptime(now, "%Y-%m-%d")
    subject = "dt="+str(now)
    mail_content = '''¡Hola!
    Mediante este correo recurrente se hace el envío de los datos agregados de las redes sociales para la actualización del dashboard
    '''
    sender_address = 'etlteam.puj@gmail.com'
    sender_pass = 'etlteam123'
    receiver_address = 'administracion@alianzacaoba.co'
    message = MIMEMultipart()
    message['From'] = sender_address
    message['To'] = receiver_address
    message['Subject'] = subject
    message.attach(MIMEText(mail_content, 'plain'))
    for file in file_list:
        print(file)
        attach_file = open(file, 'rb')
        payload = MIMEBase('application', 'octate-stream')
        payload.set_payload((attach_file).read())
        encoders.encode_base64(payload)
        filename = file.split("/")[2]
        payload.add_header('Content-Disposition', 'attachment', filename=filename)
        message.attach(payload)
    session = smtplib.SMTP('smtp.gmail.com', 587)
    session.starttls()
    session.login(sender_address, sender_pass)
    text = message.as_string()
    session.sendmail(sender_address, receiver_address, text)
    session.quit()
    print('Mail Sent')

def start_update():
    
    set_mongo_conection()
    file_list = []
    for db in defines.MONGO_DBS:
        db_name = db["db"]
        collection_name = db["collection"]
        df1 = get_dataframe(db_name, collection_name)
        store_path = create_csv(df1, db_name, collection_name)
        file_list.append(store_path)
    send_attachments(file_list)

start_update()
schedule.every().day.at("06:00").do(start_update)

while True:
    schedule.run_pending()
    sleep(1)