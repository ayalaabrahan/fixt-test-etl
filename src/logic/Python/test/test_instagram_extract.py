import sys
import instaloader
import os
import sys
file_dir = os.path.dirname(__file__)
sys.path.append(file_dir)
from producer.instagram_extract import InstagramExtract
from producer.instaloaderExtract import ExtractSvs
import unittest
import datetime
from typing import Iterable
class TestInstagramExtract(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls) -> None:
        cls.ig = InstagramExtract()
        cls.ig.set_conections()
        cls.ext = ExtractSvs.login(user = "alex.daba", password= "220600aA")
        cls.profile, cls.profile_posts = cls.ig.extract_profile("testgraphapi4")
        cls.PUBLIC_PROFILE = "testgraphapi4"
        cls.ig.account = cls.PUBLIC_PROFILE
        cls.ig.post_id= "XXXXXXXXXX"
        cls.ig.profile = cls.profile

        for i, posttt in enumerate(cls.profile_posts):
            if i == 0:
                cls.post1 = posttt
            elif i == 3:
                cls.post = posttt
                cls.post_comentarios = cls.ig.get_post_comments(cls.post)
                break
        cls.media = cls.ig.get_post_media(cls.post)
        cls.media1 = cls.ig.get_post_media(cls.post1)
        cls.dictio = cls.ig.set_post_dict(cls.post)
        cls.metadata = cls.ig.set_metadata()
        cls.dates = cls.ig.get_date_ranges()
        return super().setUpClass()

    def test01_extract_profile(self):
        self.assertIsInstance(self.profile, instaloader.structures.Profile)
        self.assertIsInstance(self.profile_posts, instaloader.NodeIterator)
        self.assertEqual(self.profile.username, "testgraphapi4")

    def test02_get_post_comments(self):
        self.assertIsInstance(self.post_comentarios, Iterable)
        self.assertEqual(self.post.comments, 10)

    def test03_post_media(self):
        self.assertEqual(self.post.typename, "GraphSidecar")
        self.assertIsInstance(self.media, dict)
        self.assertEqual(self.media['cont_img'], 2)
        self.assertEqual(self.media['cont_video'], 1)
    
    def test04_set_post_dict(self):
        self.assertIsInstance(self.dictio, dict)
        self.assertIsNot(self.dictio['Profile'], None)
        self.assertEqual(self.dictio['Profile'], "testgraphapi4")
        self.assertIsNot(self.dictio['Image links'], None)
        self.assertEqual(len(self.dictio['Hashtags used']), 2)
        self.assertIsNot(self.dictio['Location'], None)

    def test05_set_metadata(self):
        self.assertIsInstance(self.metadata, dict)
        self.assertIsNot(self.metadata['account'], None)
        self.assertIsNot(self.metadata['raw_data'], None)

    def test06_set_post_comments(self):
        self.comments_list = self.ig.set_post_comments(self.post)
        self.assertGreater(len(self.comments_list), 0)

    def test07_get_post_comments_exc(self): 
        with self.assertRaises(Exception):
            self.ig.get_post_comments("Expected to fail", 2)

    def test08_get_date_ranges(self):
        self.assertIsNone(self.dates)

    def test09_set_post_comments_exc(self):
        with self.assertRaises(Exception):
            self.ig.set_post_comments("Expected to fail", 2)

    def test10_recurrent_extraction(self):
        self.assertIsNone(self.ig.recurrent_extraction(self.dates))

    def test11_get_post_media(self):
        self.assertEqual(self.post1.typename, "GraphVideo")
        self.assertIsInstance(self.media, dict)
    
    def test12_download_post_images(self):
        self.assertIsInstance(self.ig.download_post_images(), dict)
        
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestInstagramExtract)
    unittest.TextTestRunner(verbosity=2).run(suite)