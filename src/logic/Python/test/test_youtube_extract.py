import sys
import os
file_dir = os.path.dirname(__file__)
sys.path.append(file_dir)
from producer.youtube_extract import YouTubeExtract
from googleapiclient.discovery import build
from producer.APIExtract import ExtractSvs
from producer.connection.mongo.mongo_svs import StorageSvs
import unittest
import uuid
from typing import Iterable


class TestYoutubeExtract(unittest.TestCase):
    
    @classmethod
    def setUp(cls)->None:
        cls.Video_info_sample = {"kind": "youtube#videoListResponse","etag": "CU85MKtfgb5Jjl03pdS12P-EmmY", "items": [{"kind": "youtube#video","etag": "u41SiwzVwcHai5ifrfqdAZlw_dc", "id": "GlIlrsT9cFQ", "snippet": {"publishedAt": "2021-10-12T04:19:24Z","channelId": "UCRzOyBV-0oV8U4Qg80ExoaQ","title": "Test","description": "","thumbnails": {"default": {"url": "https://i.ytimg.com/vi/GlIlrsT9cFQ/default.jpg", "width": 120, "height": 90 }, "medium": {"url": "https://i.ytimg.com/vi/GlIlrsT9cFQ/mqdefault.jpg", "width": 320, "height": 180  }, "high": { "url": "https://i.ytimg.com/vi/GlIlrsT9cFQ/hqdefault.jpg", "width": 480,"height": 360 }, "standard": {"url": "https://i.ytimg.com/vi/GlIlrsT9cFQ/sddefault.jpg","width": 640, "height": 480 }, "maxres": {"url": "https://i.ytimg.com/vi/GlIlrsT9cFQ/maxresdefault.jpg","width": 1280, "height": 720 }}, "channelTitle": "Sebastian Roberts", "categoryId": "22", "liveBroadcastContent": "none", "localized": {"title": "Test",  "description": "" } }, "contentDetails": {"duration": "PT7S", "dimension": "2d","definition": "hd","caption": "false", "licensedContent": False, "contentRating": {},"projection": "rectangular" }, "statistics": {"viewCount": "7", "likeCount": "0","dislikeCount": "0","favoriteCount": "0","commentCount": "4" } }], "pageInfo": {"totalResults": 1,"resultsPerPage": 1 }}

        cls.token = 'AIzaSyDYTccq_VK7-Y6Fq9AO39JaUhi27WBK1vU'
        cls.url = 'https://www.youtube.com/channel/UCRzOyBV-0oV8U4Qg80ExoaQ'
        cls.account = {'URL': 'https://www.youtube.com/channel/UCRzOyBV-0oV8U4Qg80ExoaQ',  'token': 'AIzaSyDYTccq_VK7-Y6Fq9AO39JaUhi27WBK1vU'}
        cls.video_id = 'GlIlrsT9cFQ'
        cls.video_url = 'https://www.youtube.com/watch?v=-OD-AJ6XKFk'
        cls.date = "2021-01-21 16:01:03"
        cls.post_id = str(uuid.uuid4())
        cls.youTube_instance = YouTubeExtract(cls.account, 'test')
        cls.youTube_instance.account = cls.account
        ExtractSvs.login(name = 'youtube', version = 'v3', developerKey = cls.token)
        cls.youTube_instance.set_conections()
        cls.youTube_instance.set_youtube_builder = build('youtube', 'v3', developerKey=cls.token)
        cls.channel_id = cls.youTube_instance.extract_channel_ID(cls.url)
        cls.channel_name = cls.youTube_instance.get_channel_name(cls.channel_id)
        cls.videos_id_list = cls.youTube_instance.get_all_video_in_channel(cls.channel_id, cls.token)
        cls.video_info = cls.youTube_instance.get_video_details(cls.video_id)
        cls.comment_count = cls.youTube_instance.get_comment_count(cls.Video_info_sample)
        cls.video_comments = cls.youTube_instance.video_comments(cls.video_id)
        cls.get_video_image = cls.youTube_instance.get_video_image(cls.video_id, cls.channel_name )
        #cls.get_channel_id = cls.youTube_instance.get_channel_id("veritasium")
        cls.is_empty_collection = cls.youTube_instance.is_empty_collection(cls.channel_name)
        #cls.get_videos_from_date = cls.youTube_instance.get_videos_from_date(cls.channel_id,cls.date )
        cls.comments_extraction_load = cls.youTube_instance.comments_extraction_load(cls.comment_count,cls.post_id, cls.video_id )
        
        #create_post_dict
        #create_video_dict
        
        return super().setUpClass()
    
    def test_extract_channel_ID(self):
       
        self.assertEqual(self.channel_id,'UCRzOyBV-0oV8U4Qg80ExoaQ')
    
    def test_get_channel_name(self):
        
        self.assertEqual(self.channel_name, 'Sebastian Roberts')

    def test_get_all_video_in_channel(self):
        self.assertTrue(True)
        #self.assertEqual(self.videos_id_list, ['-OD-AJ6XKFk', 'GlIlrsT9cFQ'])
    
    def test_get_video_details(self):
        
        self.assertEqual(self.video_info, self.Video_info_sample)

    def test_get_comment_count(self):
        
        self.assertEqual(self.comment_count, 4)

    def test_video_comments(self):
        self.assertTrue(True)
    

    def test_get_video_image(self):
        self.assertTrue(True)

    def test_get_channel_id(self):
        self.assertTrue(True)

    def test_is_empty_collection(self):
        self.assertTrue(True)

    def test_get_videos_from_date(self):
        self.assertTrue(True)
    
    def test_comments_extraction_load(self):
        self.assertTrue(True)

    def test_extract_channel_ID_exc(self):
        with self.assertRaises(Exception):
            self.youTube_instance.extract_channel_ID(1)
    
    def test_get_channel_name_exc(self):
        with self.assertRaises(Exception):
            self.youTube_instance.get_channel_name(None)

    def test_get_comment_count_exc(self):
        with self.assertRaises(Exception):
            self.youTube_instance.get_comment_count(None)

    def test_get_video_image_exc(self):
        with self.assertRaises(Exception):
            self.youTube_instance.get_video_image(None)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestYoutubeExtract)
    unittest.TextTestRunner(verbosity=2).run(suite)