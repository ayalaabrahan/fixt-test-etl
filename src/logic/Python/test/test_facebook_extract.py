
import sys
import os
import sys
import unittest

from producer.connection.mongo.mongo_svs import StorageSvs
from producer.facebook_extract import FacebookExtract
from producer.APIExtract import ExtractSvs
from typing import Iterable

file_dir = os.path.dirname(__file__)
sys.path.append(file_dir)

my_comments = [{'Post ID': '106259894838858_159679866163527', 'Comment': {'Text': '.', 'Replies': []}}, {'Post ID': '106259894838858_159679866163527', 'Comment': {'Text': 'súper!', 'Replies': []}}]
my_media_links = [{'id': '106259894838858_159679866163527', 'urls': ['https://scontent-bog1-1.xx.fbcdn.net/v/t1.6435-9/p720x720/184227674_159679842830196_3441581308272006716_n.jpg?_nc_cat=103&ccb=1-5&_nc_sid=8024bb&_nc_ohc=K13sDe3m8ZsAX8EcXRG&_nc_ht=scontent-bog1-1.xx&edm=AKK4YLsEAAAA&oh=f8c98c18237af2754147df80ae4d28e8&oe=61C7D304'], 'path': './facebook/data/106259894838858/106259894838858_159679866163527'}]

class TestFacebookExtract(unittest.TestCase):

    PAGE_ID = '106259894838858'
    PAGE_TOKEN = "EAAFP0AaHMLUBAMhGwRxnkmmrZB8UtJfKTfeZCa2LkNN6HZBEr1SyCOMHRierHMu3LhXhtVPMn6rC9CLQUrXJ7veCktlimgG1cmbKjLcfZAEYy5iktd7fhhpdr6ZB7pngqqjhVYAtAL6ZCaWp45pZCMn5oJLSZBBcYM9YiGGN0NybRz5R2L38V0sz"
    ACCOUNT = {'page_id': PAGE_ID,  'token': PAGE_TOKEN}
    ExtractSvs.login(access_token = PAGE_TOKEN)
    extracter = FacebookExtract(ACCOUNT, 'TEST')
    extracter.set_conections()
    
    @classmethod
    def setUpClass(cls) -> None:
        return super().setUpClass()

    def setUp(self) -> None:
        self.extracter.media_images_urls = []
        self.extracter.list_page_posts_metadata = []
        self.RAW_POSTS = [{'id': '106259894838858_159679866163527', 'actions': [{'name': 'Like', 'link': 'https://www.facebook.com/106259894838858/posts/159679866163527/'}, {'name': 'Comment', 'link': 'https://www.facebook.com/106259894838858/posts/159679866163527/'}, {'name': 'Share', 'link': 'https://www.facebook.com/106259894838858/posts/159679866163527/'}], 'can_reply_privately': False, 'created_time': '2021-05-13 12:00:20', 'from': {'name': 'Semillero SEKIA PUJ', 'id': '106259894838858'}, 'full_picture': 'https://scontent-bog1-1.xx.fbcdn.net/v/t1.6435-9/p720x720/184227674_159679842830196_3441581308272006716_n.jpg?_nc_cat=103&ccb=1-5&_nc_sid=8024bb&_nc_ohc=K13sDe3m8ZsAX8EcXRG&_nc_ht=scontent-bog1-1.xx&edm=AKK4YLsEAAAA&oh=f8c98c18237af2754147df80ae4d28e8&oe=61C7D304', 'icon': 'https://www.facebook.com/images/icons/photo.gif', 'instagram_eligibility': 'eligible', 'is_eligible_for_promotion': True, 'is_expired': False, 'is_hidden': False, 'is_instagram_eligible': True, 'is_popular': False, 'is_published': True, 'is_spherical': False, 'message': 'Estados Unidos ha vacunadomás de la mitad de sus adultos contra el Covid-19, pero podrían pasar meses hasta que el país haya vacunado a suficientes personas para poner la inmunidad del rebaño al alcance (y gran parte del mundo todavía está desesperadamenteesperando acceso a las vacunas).\n\nLos lugares con tasas de vacunación crecientes, como los Estados Unidos, pueden esperar que el número de casos baje mucho mientras tanto. Y antes de lo que podrías pensar. Eso se debe a que los casos disminuyen a través del principio de decadencia exponencial.\n\nMuchas personas aprendieron sobre el crecimiento exponencial en los primeros días de la pandemia para entender cómo un pequeño número de casos puede convertirse rápidamente en un brote importante a medida que se multiplican las cadenas de transmisión. La India, por ejemplo, que está en las garras de una gran crisis de Covid-19, está en una fase de crecimiento exponencial.\n\nEl crecimiento exponencial significa que el número de casos puede duplicarse en solo unos pocos días. La decadencia exponencial es su opuesto. La desintegración exponencial significa que los números de casos pueden reducirse a la mitad en la misma cantidad de tiempo.\n\n- La caries exponencial hará que las infecciones se desplomen.\n- Las infecciones comienzan a disminuir cuando se alcanza la inmunidad del rebaño.\n- Los casos volverán a subir si se levantan las restricciones demasiado pronto.\n- Los cambios en el número de casos son menos dramáticos cuando los casos son más bajos.\n\n#ciencia #tecnologia #SEKIA #sekiasemillero #news #sekianews #javeriana #ingenieria #noticias #nytimes #matematicas #comunicacion #reporte #COVID19 #coronavirus \n\nFuente: https://nyti.ms/3tBCETa', 'message_tags': [{'id': '455297511208805', 'name': '#ciencia', 'offset': 1513, 'length': 8}, {'id': '551165648240939', 'name': '#tecnologia', 'offset': 1522, 'length': 11}, {'id': '425598204192097', 'name': '#sekia', 'offset': 1534, 'length': 6}, {'id': '4700326316661241', 'name': '#sekiasemillero', 'offset': 1541, 'length': 15}, {'id': '159173667572976', 'name': '#news', 'offset': 1557, 'length': 5}, {'id': '3490154604377689', 'name': '#sekianews', 'offset': 1563, 'length': 10}, {'id': '124029737781898', 'name': '#javeriana', 'offset': 1574, 'length': 10}, {'id': '338495406270765', 'name': '#ingenieria', 'offset': 1585, 'length': 11}, {'id': '301594483303436', 'name': '#Noticias', 'offset': 1597, 'length': 9}, {'id': '518842901492357', 'name': '#nytimes', 'offset': 1607, 'length': 8}, {'id': '422439381173332', 'name': '#matematicas', 'offset': 1616, 'length': 12}, {'id': '492367034160110', 'name': '#comunicacion', 'offset': 1629, 'length': 13}, {'id': '170020079813834', 'name': '#Reporte', 'offset': 1643, 'length': 8}, {'id': '2555494087811671', 'name': '#COVID19', 'offset': 1652, 'length': 8}, {'id': '299958220132925', 'name': '#coronavirus', 'offset': 1661, 'length': 12}], 'multi_share_end_card': False, 'multi_share_optimized': False, 'permalink_url': 'https://www.facebook.com/106259894838858/posts/159679866163527/', 'privacy': {'allow': '', 'deny': '', 'description': 'Público', 'friends': '', 'value': 'EVERYONE'}, 'promotable_id': '106259894838858_159679866163527', 'promotion_status': 'inactive', 'status_type': 'added_photos', 'subscribed': True, 'timeline_visibility': 'normal', 'updated_time': '2021-11-10 19:59:01', 'video_buying_eligibility': ['INELIGIBLE_NO_VIDEO'], 'attachments': {'data': [{'media_type': 'photo', 'description': 'Estados Unidos ha vacunadomás de la mitad de sus adultos contra el Covid-19, pero podrían pasar meses hasta que el país haya vacunado a suficientes personas para poner la inmunidad del rebaño al alcance (y gran parte del mundo todavía está desesperadamenteesperando acceso a las vacunas).\n\nLos lugares con tasas de vacunación crecientes, como los Estados Unidos, pueden esperar que el número de casos baje mucho mientras tanto. Y antes de lo que podrías pensar. Eso se debe a que los casos disminuyen a través del principio de decadencia exponencial.\n\nMuchas personas aprendieron sobre el crecimiento exponencial en los primeros días de la pandemia para entender cómo un pequeño número de casos puede convertirse rápidamente en un brote importante a medida que se multiplican las cadenas de transmisión. La India, por ejemplo, que está en las garras de una gran crisis de Covid-19, está en una fase de crecimiento exponencial.\n\nEl crecimiento exponencial significa que el número de casos puede duplicarse en solo unos pocos días. La decadencia exponencial es su opuesto. La desintegración exponencial significa que los números de casos pueden reducirse a la mitad en la misma cantidad de tiempo.\n\n- La caries exponencial hará que las infecciones se desplomen.\n- Las infecciones comienzan a disminuir cuando se alcanza la inmunidad del rebaño.\n- Los casos volverán a subir si se levantan las restricciones demasiado pronto.\n- Los cambios en el número de casos son menos dramáticos cuando los casos son más bajos.\n\n#ciencia #tecnologia #SEKIA #sekiasemillero #news #sekianews #javeriana #ingenieria #noticias #nytimes #matematicas #comunicacion #reporte #COVID19 #coronavirus \n\nFuente: https://nyti.ms/3tBCETa', 'media': {'image': {'height': 720, 'src': 'https://scontent-bog1-1.xx.fbcdn.net/v/t1.6435-9/p720x720/184227674_159679842830196_3441581308272006716_n.jpg?_nc_cat=103&ccb=1-5&_nc_sid=8024bb&_nc_ohc=K13sDe3m8ZsAX8EcXRG&_nc_ht=scontent-bog1-1.xx&edm=AKK4YLsEAAAA&oh=f8c98c18237af2754147df80ae4d28e8&oe=61C7D304', 'width': 720}}, 'target': {'id': '159679839496863', 'url': 'https://www.facebook.com/sekiapuj/photos/a.106275671503947/159679839496863/?type=3'}, 'type': 'photo', 'unshimmed_url': 'https://www.facebook.com/sekiapuj/photos/a.106275671503947/159679839496863/?type=3', 'url': 'https://www.facebook.com/sekiapuj/photos/a.106275671503947/159679839496863/?type=3'}]}, 'insights': {'data': [{'name': 'post_reactions_by_type_total', 'period': 'lifetime', 'values': [{'value': {'like': 6, 'love': 1}}], 'title': 'Lifetime Total post Reactions by Type.', 'description': 'Lifetime: Total post reactions by type.', 'id': '106259894838858_159679866163527/insights/post_reactions_by_type_total/lifetime'}], 'paging': {'previous': 'https://graph.facebook.com/v11.0/106259894838858_159679866163527/insights?access_token=EAAFP0AaHMLUBAMhGwRxnkmmrZB8UtJfKTfeZCa2LkNN6HZBEr1SyCOMHRierHMu3LhXhtVPMn6rC9CLQUrXJ7veCktlimgG1cmbKjLcfZAEYy5iktd7fhhpdr6ZB7pngqqjhVYAtAL6ZCaWp45pZCMn5oJLSZBBcYM9YiGGN0NybRz5R2L38V0sz&metric=post_reactions_by_type_total&since=1637568000&until=1637740800', 'next': 'https://graph.facebook.com/v11.0/106259894838858_159679866163527/insights?access_token=EAAFP0AaHMLUBAMhGwRxnkmmrZB8UtJfKTfeZCa2LkNN6HZBEr1SyCOMHRierHMu3LhXhtVPMn6rC9CLQUrXJ7veCktlimgG1cmbKjLcfZAEYy5iktd7fhhpdr6ZB7pngqqjhVYAtAL6ZCaWp45pZCMn5oJLSZBBcYM9YiGGN0NybRz5R2L38V0sz&metric=post_reactions_by_type_total&since=1637913600&until=1638086400'}}, 'shares': {'count': 1}, 'comments': {'data': [{'message': '.', 'comments': {'data': [], 'summary': {'order': 'chronological', 'total_count': 0, 'can_comment': True}}, 'id': '159679866163527_218835920247921'}, {'message': 'súper!', 'comments': {'data': [], 'summary': {'order': 'chronological', 'total_count': 0, 'can_comment': True}}, 'id': '159679866163527_292992682832244'}], 'paging': {'cursors': {'before': 'MgZDZD', 'after': 'MQZDZD'}}, 'summary': {'order': 'ranked', 'total_count': 2, 'can_comment': True}}}]
        return super().setUp()
    
    def test_extract_facebook_page_name(self):
        page_name = self.extracter.extract_facebook_page_name()
        self.assertEqual(page_name, 'Semillero SEKIA PUJ')

    def test_extract_facebook_page_posts(self):
        posts = self.extracter.extract_facebook_page_posts()
        self.assertIsInstance(posts, Iterable)
        self.assertGreaterEqual(len(posts), 0)
    
    def test_generate_metadata_posts(self):
        media_type = {'text': 1, 'photo': 1, 'video': 0}
        raw_post = self.extracter.generate_metadata_posts(self.RAW_POSTS)[0]
        self.assertEqual(raw_post['media_type'], media_type)
        self.assertEqual(raw_post['raw_data']['count_comments'], self.RAW_POSTS[0]['count_comments'])
    
    def test_generate_metadata_post(self):
        raw_post = self.extracter.generate_metadata_post(self.RAW_POSTS[0])
        media_type = {'text': 1, 'photo': 1, 'video': 0}
        self.assertEqual(raw_post['media_type'], media_type)
        self.assertEqual(raw_post['raw_data']['count_comments'], self.RAW_POSTS[0]['count_comments'])
    
    @unittest.skipIf(True, 'coverage by unnittest $test_generate_metadata_post$')
    def test_find_total_metadata_type(self):
        count_img, count_video, count_text = self.extracter.find_total_metadata_type(
            self.RAW_POSTS[0])
        self.assertEqual(count_text, 1)
        self.assertEqual(count_img, 1)
        self.assertEqual(count_video, 0)
    
    def test_get_essential_comments(self):
        current_post = self.RAW_POSTS[0]
        self.extracter.get_essential_comments(current_post)
        self.assertEqual(len(self.extracter.metadata_comments), 2)
        self.assertEqual(my_comments, self.extracter.metadata_comments)
    
    def test_fix_time_zone(self):
        self.extracter.fix_time_zone(self.RAW_POSTS[0])
        self.assertEqual(self.RAW_POSTS[0]['created_time'], '2021-05-13 07:00:20')
        self.assertEqual(self.RAW_POSTS[0]['updated_time'], '2021-11-10 14:59:01')
        
    def test_find_total_metadata_type_ex(self):
        with self.assertRaises(Exception):
            self.extracter.find_total_metadata_type(None)
            
    def test_get_essential_media_images(self):
        current_post = self.RAW_POSTS[0]
        self.extracter.get_essential_media_images(current_post)
        self.assertEqual(my_media_links, self.extracter.media_images_urls)
        
    def test_append_attachment(self):
        current_post = self.RAW_POSTS[0]
        attachments = current_post['attachments']['data'][0]
        post_id = current_post['id']
        urls =[{'id': '106259894838858_159679866163527', 'urls': my_media_links[0]['urls'], 'path': './facebook/data/106259894838858/106259894838858_159679866163527'}]
        self.extracter.append_attachment(attachments, post_id)
        self.assertEqual(urls, self.extracter.media_images_urls)
        
    @unittest.skipIf( not StorageSvs.exist_database('Facebook'), 'No database in Mongo')
    def test_get_posts_essential(self):
        esensial_comments = self.extracter.get_posts_essential()
        self.assertEqual(esensial_comments, my_comments)
    
    @unittest.skipIf( not StorageSvs.exist_database('Facebook'), 'No database in Mongo')
    def test_add_request_date_params(self):
        self.extracter.add_request_date_params()
        self.assertEqual(int(self.extracter.request_post_params['until']/100000), 16381)
    
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestFacebookExtract)
    unittest.TextTestRunner(verbosity=2).run(suite)
