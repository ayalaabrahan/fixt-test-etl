from producer.connection.APIExtract.IAPIExtract import IAPIExtract
import facebook


class ExtractSvs(IAPIExtract):

    __graph = None

    def extract_post(*args, **kwargs):
        return ExtractSvs.__graph.get_all_connections(id = kwargs['id'], connection_name = kwargs['connection_name'], **kwargs['params'])

    def extract_profile(*args, **kwargs):
        return ExtractSvs.__graph.get_object(id = kwargs['id'], fields = kwargs['fields'])

    def login(*args, **kwargs):
        ExtractSvs.__graph = facebook.GraphAPI(access_token = kwargs['access_token'], version ="8.0")
