from pymongo import MongoClient
from kafka import KafkaProducer
from time import sleep
from datetime import datetime, timedelta
import traceback
import schedule
import json
import concurrent.futures

from dateutil.parser import parse

import producer.credentials
import producer.defines as defines
from producer.connection.Extracter.extracter import IExtracter
from producer.connection.kafka.kafka_svs import StreamerSvs
from producer.connection.mongo.mongo_svs import StorageSvs
from producer.util.downloads.download import DowloadSvs
from producer.APIExtract import ExtractSvs
from producer.util.logs.log import LogSvs

ALWAYS = True
TOKEN = 'token'
ID = 'id'
PAGE_ID = 'page_id'
NAME = 'name'
RAW_DATA = 'raw_data'
QUERY_DATE = 'query_date'
POST_COLLECTION_INDEX = 0
COMMENT_COLLECTION_INDEX = 1
FEED = 'feed'
ALL_POST_FIELDS = 'id,actions,can_reply_privately,created_time,from,full_picture,icon,instagram_eligibility,is_eligible_for_promotion,is_expired,is_hidden,is_instagram_eligible,is_popular,is_published,is_spherical,message,message_tags,multi_share_end_card,multi_share_optimized,permalink_url,privacy,promotable_id,promotion_status,status_type,subscribed,timeline_visibility,updated_time,video_buying_eligibility,attachments{media_type,subattachments.limit(100),description,description_tags,media,target,title,type,unshimmed_url,url},insights.metric(post_reactions_by_type_total),shares{count},comments.summary(true){ message,comments.summary(true){ message} }'
TEST_ALL_POST_FIELDS = 'id,permalink_url,message,attachments{media_type,subattachments,description,description_tags,media,target,title,type,unshimmed_url,url},insights.metric(post_reactions_by_type_total),shares{count},comments.summary(true){ message,comments.summary(true){ message} }'

class FacebookExtract(IExtracter):

    media_images_urls = list()
    metadata_comments = list()
    img_urls = []
    request_post_params = {"fields": ALL_POST_FIELDS}

    def __init__(self, account, environment):
        self.account = account
        self.initialize_deamon() if environment == 'PRODUCTION' else None

    def initialize_deamon(self):# pragma: no cover
        print("init")
        self.set_conections()
        self.set_graph_api()
        self.start_extraction()
        self.store_data()
        print("before send kafka")
        self.send_kafka()
        
    def set_conections(self):
        self.set_kafka_conection()
        self.set_mongo_conection()

    def set_kafka_conection(self):
        try:
            kafka_host = defines.KAFKA_HOST
            kafka_port = defines.KAFKA_PORT
            encoding = defines.ENCODE
            StreamerSvs.create_producer(kafka_host, kafka_port, encoding)
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="set_kafka_conection", account=self.account)

    def set_mongo_conection(self):
        try:
            mongo_host = defines.MONGO_HOST
            mongo_port = defines.MONGO_PORT
            StorageSvs.create_client(mongo_host, mongo_port)
            self.set_mongo_collections()
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="set_mongo_conection", account=self.account)

    def set_mongo_collections(self):
        try:
            mongo_db_name = defines.MONGO_DATABASE_NAME
            mongo_collections_names = defines.MONGO_COLLECTIONS
            StorageSvs.create_collections(
                mongo_db_name, mongo_collections_names)
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="set_mongo_collections", account=self.account)

    def set_graph_api(self):# pragma: no cover
        try:
            account_token = self.account[TOKEN]
            ExtractSvs.login(access_token=account_token)
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="set_graph_api", account=self.account)

    def start_extraction(self):# pragma: no cover
        self.extract_facebook_page_posts()
        self.get_posts_essential()

    def get_last_extraction_timestamp(self):  # TODO: TEST
        try:
            last_query_date = StorageSvs.find_one(defines.POSTS_COLLECTION_NAME, {"account.query_value": self.account[PAGE_ID]}, {"query_date": 1}, [('query_date', -1)])
            last_extracted_timestamp = parse(last_query_date['query_date'])
            return last_extracted_timestamp.timestamp()
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="get_last_extraction_timestamp", account=self.account)

    def add_request_date_params(self):  # TODO: TEST
        try:
            today = self.get_today_timestamp()
            last_extracted_date = self.get_last_extraction_timestamp()
            if(int(last_extracted_date) != int(today)):
                self.request_post_params.update(
                    since = int(last_extracted_date), until = int(today))
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="add_request_date_params", account=self.account)

    def get_today_timestamp(self):  # TODO: TEST
        try:
            today = parse(str(datetime.now()))
            return int(today.timestamp())
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="get_today_timestamp", account=self.account)

    def store_data(self):# pragma: no cover
        self.store_media_images()
        self.store_comments()
        self.generate_metadata_posts(self.list_extracted_page_posts)
        self.store_posts()

    def extract_facebook_page_name(self):
        try:
            page_info = ExtractSvs.extract_profile(
                id=self.account[PAGE_ID], fields=NAME)
            self.facebook_page_name = page_info[NAME]
            return self.facebook_page_name
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="extract_facebook_page_name", account=self.account)

    def extract_facebook_page_posts(self):  # TODO: decouple
        try:
            self.add_request_date_params() if self.are_there_stored_posts() else None
            self.list_extracted_page_posts = list(self.extract_from_feed())
            return self.list_extracted_page_posts
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="extract_facebook_page_posts", account=self.account)

    def are_there_stored_posts(self):
        try:
            documents_quantity = StorageSvs.count(defines.POSTS_COLLECTION_NAME, {"account.query_value": self.account[PAGE_ID]})
            are_there_stored_posts = bool(documents_quantity)
            return are_there_stored_posts
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="are_there_stored_posts", account=self.account)

    def extract_from_feed(self):
        try:
            *generator_response, = ExtractSvs.extract_post(
                id=self.account[PAGE_ID], connection_name=FEED, params=self.request_post_params)
            return generator_response
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="extract_from_feed", account=self.account)

    def find_total_metadata_type(self, current_post):
        try:
            count_video = 0
            count_text = 0
            count_img = 0
            if 'message' in current_post:
                count_text = 1
            if 'attachments' in current_post:
                count_img = 1
                attachments = current_post['attachments']['data'][0]
                if attachments['media_type'] == 'link' or attachments['media_type'] == 'event':
                    count_text += 1
                elif attachments['media_type'] == 'album':
                    count_img = len(attachments['subattachments']['data'])
                elif attachments['media_type'] == 'video':
                    count_video = 1
            return count_img, count_video, count_text
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="find_total_metadata_type", account=self.account)

    # TODO: change method name generate -> add
    def generate_metadata_posts(self, raw_posts):
        try:
            self.list_page_posts_metadata = []
            for current_post in raw_posts:
                self.list_page_posts_metadata.append(
                    self.generate_metadata_post(current_post))
            return self.list_page_posts_metadata
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="generate_metadata_posts", account=self.account)

    # TODO: change method name generate -> add
    def generate_metadata_post(self, current_post):
        
        try:
            count_img, count_video, count_text = self.find_total_metadata_type(
                current_post)
            self.facebook_page_name = self.extract_facebook_page_name()
            comments =  len(current_post.pop('comments', None)['data'])
            current_post.update({'count_comments':comments})
            post_metadata = {
                'uuid': current_post['id'],
                'status': 'raw',
                'query_date': str(datetime.now().strftime(defines.DATE_FORMAT)),
                'last_status_date': str(datetime.now().strftime(defines.DATE_FORMAT)),
                'social_network': "Facebook",
                'account': {
                    "query_value": self.account[PAGE_ID],
                    "query_description": self.facebook_page_name
                },
                "media_type": {
                    "text": count_text,
                    "photo": count_img,
                    "video": count_video
                },
                'images_path': './facebook/data/'+self.account[PAGE_ID]+'/'+current_post['id'],
                'raw_data': current_post
            }
            return post_metadata
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="generate_metadata_post", account=self.account)

    def store_comments(self):# pragma: no cover
        try:
            if len(self.metadata_comments) > 0:
                StorageSvs.store(
                    defines.POSTS_COMMENTS_COLLECTION_NAME, self.metadata_comments)
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="store_comments", account=self.account)

    def store_posts(self):# pragma: no cover
        try:
            if len(self.list_page_posts_metadata) > 0:
                StorageSvs.store(defines.POSTS_COLLECTION_NAME, self.list_page_posts_metadata)
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="store_posts", account=self.account)

    def store_media_images(self):# pragma: no cover
        try:
            DowloadSvs.download(self.media_images_urls)
        except Exception as e:
            LogSvs.add_error(type = type(e), traceback = traceback.format_exc(), function = "concurrent_download", account = self.account)
   
    def get_posts_essential(self):
        try:
            for current_post in self.list_extracted_page_posts:
                self.get_essential_comments(current_post)
                self.fix_time_zone(current_post)
                self.get_essential_media_images(
                    current_post) if 'attachments' in current_post else None
            return self.metadata_comments
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="get_posts_essential", account=self.account)

    def fix_time_zone(self, current_post):
        try:
            current_post['created_time'] = self.format_date(
                current_post['created_time'])
            current_post['updated_time'] = self.format_date(
                current_post['updated_time'])
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="fix_time_zone", account=self.account)

    def format_date(self, date):
        try:
            date = str(parse(date))
            date = date.replace("+00:00", "")
            date = self.change_time_zone(date)
            return str(date)
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="error rename_date", account=self.account)

    def change_time_zone(self, date):
        try:
            date_time_obj = datetime.strptime(date, defines.DATE_FORMAT)
            hours = 5
            hours_added = timedelta(hours=hours)
            future_date_and_time = date_time_obj - hours_added
            return future_date_and_time
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="change_time_zone", account=self.account)

    def get_essential_comments(self, current_post):
        try:
            post_id = current_post['id']
            comments = current_post['comments']
            for current_comment in comments['data']:
                self.append_comment(current_comment, post_id)
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="get_essential_comments", account=self.account)

    def append_comment(self, comment, post_id):
        try:
            metadata_comment = self.add_metadata_comment(comment, post_id)
            self.metadata_comments.append(metadata_comment)
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="append_comment", account=self.account)

    def add_metadata_comment(self, current_comment, post_id):  # TODO: validation replies
        try:
            current_comment_message = current_comment['message']
            current_comment_replies = current_comment['comments']['data']
            metadata_comment = {
                "Post ID": post_id,
                "Comment": {
                    "Text": current_comment_message,
                    "Replies": current_comment_replies
                }
            }
            return metadata_comment
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="add_metadata_comment", account=self.account)

    def get_essential_media_images(self, current_post):
        try:
            self.img_urls = []
            attachments = current_post['attachments']['data'][0]
            post_id = current_post['id']
            self.append_attachment(
                attachments, post_id) if not 'subattachments' in attachments else self.append_subattachments(attachments, post_id)
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="error get_essential_media_images", account=self.account)

    def append_attachment(self, attachment, post_id):
        try:
            if 'media' in attachment and attachment['media_type'] != 'video':
                self.append_img_url(attachment)
                self.append_img_urls(post_id)
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="append_attachment", account=self.account)

    def append_subattachments(self, attachments, post_id):
        try:
            subattachments = attachments['subattachments']['data']
            for attachment in subattachments:
                self.append_img_url(attachment)
            self.append_img_urls(post_id)
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="append_subattachments", account=self.account)

    def append_img_url(self, attachment):
        try:
            img_url = self.get_img_url(attachment)
            self.img_urls.append(img_url)
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="append_img_url", account=self.account)

    def get_img_url(self, attachment):
        try:
            if 'media' in attachment:
                img_url = attachment['media']['image']['src']
                return img_url
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="get_img_url", account=self.account)

    def append_img_urls(self, post_id):
        try:
            path = './facebook/data/' + \
                self.account[PAGE_ID] + '/' + str(post_id)
            img_data = dict(id=post_id, urls=self.img_urls, path=path)
            self.media_images_urls.append(img_data)
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="append_img_urls", account=self.account)

    def send_kafka(self):# pragma: no cover
        try:
            print(self.list_page_posts_metadata)
            StreamerSvs.publish(self.list_page_posts_metadata,
                                defines.KAFKA_POSTS_TOPIC)
            StreamerSvs.publish(self.metadata_comments,
                                defines.KAFKA_COMMENTS_TOPIC)
        except Exception as e:
            LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
            ), function="send_kafka", account=self.account)


def start_concurrent_extraction(account):# pragma: no cover
    try:
        FacebookExtract(account, 'PRODUCTION')
    except Exception as e:
        LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
        ), function="start_concurrent_extraction")


def start_concurrent_extractions(): # pragma: no cover
    try:
        with concurrent.futures.ProcessPoolExecutor() as executor:
            executor.map(start_concurrent_extraction,producer.credentials.ACCOUNTS)
    except Exception as e:
        LogSvs.add_error(type=type(e), traceback=traceback.format_exc(
        ), function="start_concurrent_extractions")


if __name__ == '__main__': # pragma: no cover

    start_concurrent_extractions()

    """ Ejecución programada """
    schedule.every().day.at("03:00").do(start_concurrent_extractions)

    while ALWAYS:
        schedule.run_pending()
        sleep(1)
