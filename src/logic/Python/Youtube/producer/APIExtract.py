from producer.connection.APIExtract.IAPIExtract import IAPIExtract
from googleapiclient.discovery import build


class ExtractSvs(IAPIExtract):

    __youtube = None

    def extract_post(*args, **kwargs):
        pass

    def login(*args, **kwargs):
        ExtractSvs.__youtube = build(kwargs['name'], kwargs['version'], developerKey = kwargs['developerKey'])

    def extract_profile(*args, **kwargs):
        return ExtractSvs.__youtube.channels().list(id = kwargs['id'], part = kwargs['part']).execute()
    
    def extract_videos_ids(*args, **kwargs):
        return ExtractSvs.__youtube.search().list(part=kwargs['part'], channelId=kwargs['channelId'], order=kwargs['order']).execute()
    
    def extract_videos_details(*args, **kwargs):
        return ExtractSvs.__youtube.videos().list(part = kwargs['part'], id =kwargs['id']).execute()

    def extract_videos_ids_by_date(*args, **kwargs):
        return ExtractSvs.__youtube.search().list(part=kwargs['part'],channelId=kwargs['channelId'], publishedAfter=kwargs['publishedAfter']).execute()
    
    def extract_comment(*args, **kwargs):
        return ExtractSvs.__youtube.commentThreads().list(part = kwargs['part'],videoId=kwargs['videoId']).execute()
