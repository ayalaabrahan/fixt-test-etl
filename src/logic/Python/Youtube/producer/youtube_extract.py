import json
from time import sleep
from urllib.parse import urlparse, parse_qs
from datetime import datetime
from datetime import timedelta
from dateutil.parser import parse
import producer.defines as defines
import producer.credentials as credentials
import schedule
import uuid
import urllib.request
import concurrent.futures
import pymongo
import traceback

from producer.util.downloads.download import DowloadSvs
from producer.connection.Extracter.extracter import IExtracter
from producer.connection.kafka.kafka_svs import StreamerSvs
from producer.connection.mongo.mongo_svs import StorageSvs
from producer.APIExtract import ExtractSvs
from producer.util.logs.log import LogSvs


ALWAYS = True
TOKEN = 'token'
url = 'url'
class YouTubeExtract(IExtracter):
    comments = []
    def __init__(self, account, environment): #pragma: no cover
        try:
            if environment == "Production":
                print(account)
                self.account = account
                self.initialize()            
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "__init__", account = self.account)
    
    def initialize(self): #pragma: no cover
        self.set_conections()
        self.set_youtube_builder()
        self.start_extract()


    def set_youtube_builder(self):
        ExtractSvs.login(name = 'youtube', version = 'v3', developerKey = self.account[TOKEN])

    def start_extract(self): #pragma: no cover
        self.call(self.account['URL'])

    def video_comments(self, video_id):
        try:
            replies = []
            video_response = ExtractSvs.extract_comment(part="snippet,replies", videoId=video_id)
            print(video_response)

            for item in video_response['items']:
                comment = item['snippet']['topLevelComment']['snippet']['textDisplay']
                replycount = item['snippet']['totalReplyCount']
                if replycount > 0:                    
                    for reply in item['replies']['comments']:
                        reply = reply['snippet']['textDisplay']
                        replies.append(reply)
                comments_dict = {
                            "Text": str(comment),
                            "Replies": replies
                            }
                replies = []

            return comments_dict
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "video_comments", account = self.account)

    def get_video_image(self, video_id, channel_name):
        try:
            media_images_urls = list()
            pic_url = 'https://img.youtube.com/vi/'+video_id+'/0.jpg'
            path = './Youtube/data/' + channel_name +"/"+ video_id
            pic_urls = []
            pic_urls.append(pic_url)
            img_data = dict( id = video_id, urls = pic_urls, path = path)
            media_images_urls.append(img_data) 
            DowloadSvs.download(media_images_urls)

        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "get_video_image", account = self.account)

    def get_channel_id(self, channel_name):
        try:
            search_channel_name = channel_name
            channels_response = ExtractSvs.extract_profile(forUsername = search_channel_name, part = 'id')
            information = channels_response['items']
            aux = information[0]
            return str(aux['id'])
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "get_channel_id", account = self.account)

    def get_channel_name(self, channel_id):
        try:
            search_channel_name = channel_id
            channels_response =  ExtractSvs.extract_profile(id = search_channel_name, part = 'snippet')
            information = channels_response['items']
            aux = information[0]
            snippet = aux['snippet']
            return str(snippet['title'])
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "get_channel_name", account = self.account)

    def get_all_video_in_channel(self,channel_id, token):
        try:
            base_search_url = 'https://www.googleapis.com/youtube/v3/search?'

            first_url = base_search_url+'key={}&channelId={}&part=snippet,id&order=date&maxResults=25'.format(token, channel_id)

            video_links = []
            url = first_url
            keep_going = True
            while keep_going:
                inp = urllib.request.urlopen(url)
                resp = json.load(inp)
                for i in resp['items']:
                    if i['id']['kind'] == "youtube#video":
                        video_links.append(i['id']['videoId'])
                try:
                    next_page_token = resp['nextPageToken']
                    url = first_url + '&pageToken={}'.format(next_page_token)
                except:
                    break
            return video_links
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "get_all_video_in_channel", account = self.account)

    def extract_channel_ID(self, url):
        try:
            query = urlparse(url)
            if query.hostname in {'www.youtube.com', 'youtube.com'}:

                if query.path[:6] == '/user/':
                    return self.get_channel_id(query.path.split('/')[2])

                if query.path[:3] == '/c/':
                    return self.get_channel_id(query.path.split('/')[2])

                if query.path[:9] == '/channel/':
                    return query.path.split('/')[2]

        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "extract_channel_ID", account = self.account)

    def get_video_details(self, video_id):
        try:
            return ExtractSvs.extract_videos_details(part = "snippet,contentDetails,statistics", id = video_id)
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "get_video_details", account = self.account)

    def is_empty_collection(self, channel_name):
        try:
            posts_verify =  StorageSvs.find_one(defines.POSTS_COLLECTION_NAME, {"account.query_description":channel_name},{"query_date":1}, [('query_date',-1)])
                
            if posts_verify == None:
                print("Coleccion vacia: ", True)
                return True
            else :
                print("Coleccion vacia: ", False)
                return False
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "is_empty_collection", account = self.account)

    def rename_date(self, items):
        try:
            date = items["snippet"]["publishedAt"]
            date = str(parse(date))
            date = date.replace("T", "")
            date = date.replace("Z", "")
            date = date[:-6]
            date = self.update_timezone(date)
            items["snippet"]["publishedAt"] = str(date)
            return items
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "rename_date", account = self.account)

    def update_timezone(self, date):
        try:
            date_time_obj = datetime.strptime(date, defines.DATE_FORMAT)
            hours = 5
            hours_added = timedelta(hours = hours)
            future_date_and_time = date_time_obj - hours_added
            return future_date_and_time
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "update_timezone", account = self.account)
    
    def get_latest_extraction_date(self, channel_name):
        try:
            last_query_date = StorageSvs.find_one(defines.POSTS_COLLECTION_NAME, {"account.query_description":channel_name},{"query_date":1}, [('query_date',-1)])
            last_extracted_timestamp = str(parse(last_query_date['query_date']))
            date_time_obj = datetime.strptime(last_extracted_timestamp, defines.DATE_FORMAT)
            hours = 5
            hours_added = timedelta(hours = hours)

            future_date_and_time = date_time_obj + hours_added
            print(future_date_and_time)
            future_date_and_time = str(future_date_and_time)
            last_extracted_date = future_date_and_time.replace(" ", "T")
            last_extracted_date = last_extracted_date + "Z"
            print(last_extracted_date)
            return last_extracted_date
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "get_latest_extraction_date", account = self.account)
    
    def get_videos_from_date(self,channel_id, last_extracted_date ):
        try:
            videos_list_pre = ExtractSvs.extract_videos_ids_by_date(part="id",channel_id=channel_id, publishedAfter=last_extracted_date)
            print(videos_list_pre)
            items = videos_list_pre.get("items")
            videos_list = []     
            for i in items:
                video_id = i.get("id")
                video_id = video_id.get("videoId")
                if (video_id is not None):
                    videos_list.append(video_id)
                print(video_id)
            
            print("videos List:")
            print(videos_list)
            return videos_list
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "get_videos_from_date", account = self.account)
    

    def send_comment_kafka(self, comments_list):
        try:
            StreamerSvs.publish(comments_list, defines.COMMENTS_QUEUE)
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "send_comment_kafka", account = self.account)

    def set_conections(self):
        try:
            self.set_kafka_conection()
            self.set_mongo_conection()
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "set_conections", account = self.account)

    def set_kafka_conection(self):
        try:
            kafka_host = defines.KAFKA_HOST
            kafka_port = defines.KAFKA_PORT
            encoding = defines.ENCODE
            StreamerSvs.create_producer(kafka_host, kafka_port, encoding)
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "set_kafka_conection", account = self.account)

    def set_mongo_conection(self):
        try:
            mongo_host = defines.MONGO_HOST
            mongo_port = defines.MONGO_PORT
            StorageSvs.create_client(mongo_host, mongo_port)
            self.set_mongo_collections()
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "set_mongo_conection", account = self.account)

    def set_mongo_collections(self):
        try:
            mongo_db_name = defines.MONGO_DATABASE_NAME
            mongo_collections_names = defines.MONGO_COLLECTIONS
            StorageSvs.create_collections(mongo_db_name, mongo_collections_names)
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "set_mongo_collections", account = self.account)

    def validate_database(self, channel_name, channel_id): #pragma: no cover
        try:
            if self.is_empty_collection(channel_name):
                print("Ejecucion inicial (BD vacia)")
                videos_list = self.get_all_video_in_channel(channel_id, self.account[TOKEN])

            else:
                print("Ejecucion NO inicial (BD con info)")
                last_extracted_date = self.get_latest_extraction_date(channel_name)
                videos_list = self.get_videos_from_date(channel_id, last_extracted_date)
            return videos_list
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "validate_database", account = self.account)

    def get_comment_count(self, video_info):
        try:
            items = video_info.get("items")[0]
            items = self.rename_date(items)
            statistics = items["statistics"]
            comment_count = statistics["commentCount"]
            return comment_count
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "get_comment_count", account = self.account)

    def comments_extraction_load(self, comment_count, post_id, id):
        try:
            all_comments = []
            if(int(comment_count) > 0):
                comments = self.video_comments(id)
                comentario = {
                        "Post ID": post_id,
                        "Comment": comments    
                        }
                all_comments.append(comentario)
                try:
                    StorageSvs.store(defines.POSTS_COMMENTS_COLLECTION_NAME, all_comments)
                    self.send_comment_kafka(all_comments)
                except pymongo.errors.BulkWriteError as e:
                    print (e.details['writeErrors'])
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "comments_extraction_load", account = self.account)

    def create_post_dict(self, post_id, channel_name, video, video_id):
        try:
            post_dict = {    'uuid': post_id,
                        'status': 'raw',
                        'query_date': str(datetime.now().strftime(defines.DATE_FORMAT)),
                        'last_status_date' : str(datetime.now().strftime(defines.DATE_FORMAT)),
                        'social_network': "YouTube",
                        'account':{ "query_value": url,
                                    "query_description": channel_name},
                        "media_type": {"text" :1,
                                    "photo":1,
                                    "video":1},
                        "images_path": "./Youtube/data/" + channel_name +"/"+ video_id,
                        'raw_data': video
                    }
            return post_dict      
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "create_post_dict", account = self.account)

    def create_video_dict(self, video_id, video_info):
        try:
            video = {
                    'id' : video_id,
                    'Information' : video_info
                }
            return video
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "create_video_dict", account = self.account)


    def call(self, url): #pragma: no cover
        try:
            channel_id = self.extract_channel_ID(url)
            channel_name = self.get_channel_name(channel_id)
            videos_list = self.validate_database(channel_name, channel_id)

            print('-------------------------------------------Nombre: '+channel_name+" Tamano " + str(len(videos_list)))
            post_id = str(uuid.uuid4())
            
            for video_id in videos_list:
                self.get_video_image(video_id, channel_name)
                video_info = self.get_video_details(video_id)
                comment_count = self.get_comment_count(video_info)
                self.comments_extraction_load(comment_count, post_id, video_id)
                print("  "+ channel_name +" video: "+video_id)
                    
                video = self.create_video_dict(video_id, video_info)
                post_dict = self.create_post_dict(post_id, channel_name, video, video_id)

                post = list()
                post.append(post_dict)
                StorageSvs.store(defines.POSTS_COLLECTION_NAME, post)
                StreamerSvs.publish(post,defines.POSTS_QUEUE)
                
            print("fin")
        except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "call", account = self.account)


def start_concurrent_extraction(account): #pragma: no cover
    try:
        YouTubeExtract(account, "Production")
    except Exception as e:
                LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "call", account = account)

def start_concurrent_extractions(): #pragma: no cover
    try:
        with concurrent.futures.ThreadPoolExecutor() as executor:
            executor.map(start_concurrent_extraction, credentials.ACCOUNTS)
        
    except Exception as e:
            LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "call")
if __name__ == '__main__':
    start_concurrent_extractions()
    """ Ejecución programada """ 
    schedule.every().day.at("03:00").do(start_concurrent_extractions)
    while ALWAYS:
        schedule.run_pending()
        sleep(1) 
