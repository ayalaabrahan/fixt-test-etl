MONGO_DATABASE_NAME = 'Instagram'
COLLECTION_NAME = 'instagram_raw'
COLLECTION_COMMENTS_NAME = 'instagram_comments'
LOGS_COLLECTION = "social_network_errors"
MONGO_HOST = 'mongo'
MONGO_PORT = '27017'
MONGO_COLLECTIONS = [COLLECTION_NAME, COLLECTION_COMMENTS_NAME, LOGS_COLLECTION]
KAFKA_HOST = 'kafka'
KAFKA_PORT = '9092'
ENCODE = 'utf-8'
POSTS_QUEUE = 'instagram_queue'
COMMENTS_QUEUE = 'instagram_comments_queue'
CLIENT_USERNAMES = ['unijaveriana', 'confesiones_javeriana']

DATE_FORMAT = '%Y-%m-%d %H:%M:%S'
PROFILE_QUERY = "raw_data.Profile"
