from producer.connection.APIExtract.IAPIExtract import IAPIExtract
from time import sleep
import instaloader
import random

class MyRateController(instaloader.RateController):
    def wait_before_query(self, query_type):
        sleep_per_req = random.randint(4, 8)
        sleep(sleep_per_req)

    def count_per_sliding_window(self, query_type):
        return 60

class ExtractSvs(IAPIExtract):

    __L = None

    def extract_profile(*args, **kwargs):
        sleep(random.randint(2,6))
        profile = instaloader.Profile.from_username(ExtractSvs.__L.context, kwargs['accountX'])
        return profile, profile.get_posts()

    def extract_comment(*args, **kwargs):
        post = kwargs['postX']
        print("before")
        com = post.get_comments()
        return com
    
    def extract_comment_reply(*args, **kwargs):
        replies = []
        comment = kwargs['commentX']
        for answer in comment.answers:
            replies.append(str(answer))
        return replies

    def extract_post_slides(*args, **kwargs):
        sleep(random.randint(2,6))
        post = kwargs['postX']
        return post.get_sidecar_nodes()

    def login(*args, **kwargs):
        sleep(random.randint(2,6))
        ExtractSvs.__L = instaloader.Instaloader(request_timeout=60.0, rate_controller=lambda ctx: MyRateController(ctx))
        # L.load_session_from_file(defines.LOGIN_USER_1,"/root/.config/instaloader/session-testgraphapi4")
        ExtractSvs.__L.login(kwargs['user'], kwargs['password'])
        