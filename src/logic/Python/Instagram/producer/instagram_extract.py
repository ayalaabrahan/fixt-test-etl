import producer.defines
import producer.credentials as credentials
import time
import schedule
import uuid
import traceback
import logging
import emoji
from time import sleep
from datetime import datetime
from itertools import dropwhile, takewhile
from contextlib import contextmanager
from producer.instaloaderExtract import ExtractSvs
from producer.connection.Extracter.extracter import IExtracter
from producer.util.downloads.download import DowloadSvs
from producer.connection.kafka.kafka_svs import StreamerSvs
from producer.connection.mongo.mongo_svs import StorageSvs
from producer.util.logs.log import LogSvs

class InstagramExtract(IExtracter):

    post_comments_list= None

    def set_conections(self):
        self.set_kafka_conection()
        self.set_mongo_conection()

    def set_kafka_conection(self):
        kafka_host = producer.defines.KAFKA_HOST
        kafka_port = producer.defines.KAFKA_PORT
        encoding = producer.defines.ENCODE
        StreamerSvs.create_producer(kafka_host, kafka_port, encoding)

    def set_mongo_conection(self):
        mongo_host = producer.defines.MONGO_HOST
        mongo_port = producer.defines.MONGO_PORT
        StorageSvs.create_client(mongo_host, mongo_port)
        self.set_mongo_collections()

    def set_mongo_collections(self):
        mongo_db_name = producer.defines.MONGO_DATABASE_NAME
        mongo_collections_names = producer.defines.MONGO_COLLECTIONS
        StorageSvs.create_collections(mongo_db_name, mongo_collections_names)

    def set_instaloader(self):
        ExtractSvs.login(user = credentials.LOGIN_USER_1, password= credentials.LOGIN_PASS_1)

    def set_api(self):
        self.set_instaloader()

    def extract_profile(self, account_name):
        profile, profile_posts = ExtractSvs.extract_profile(accountX = account_name)
        return profile, profile_posts

    def start_etl(self): # pragma: no cover
        for cuenta in producer.defines.CLIENT_USERNAMES:
            self.account = cuenta
            print("*************Get posts 7 with ", self.account, " account*********************")
            self.profile, self.profile_posts = self.extract_profile(self.account)
            dates = self.get_date_ranges()
            if dates == None:
                self.first_extraction()
            else:
                self.recurrent_extraction(dates)

    def first_extraction(self):
        print("Not post found yet for ", self.account, " account")
        for i, post in enumerate(self.profile_posts):
            try:
                print("Extracting post #", i ," published at ", post.date_local)
                self.get_post_contents(post)
            except Exception as e:
                LogSvs.add_error(type = type(e).__name__, traceback = traceback.format_exc(), function = "first_extraction", account = self.account)

    def recurrent_extraction(self, dates):
        if dates is not None:
            until = dates['latest_post_date']
            since = dates['first_post_date']
            now = datetime.now().strftime(producer.defines.DATE_FORMAT)
            now = datetime.strptime(now, producer.defines.DATE_FORMAT)
            old_date = datetime(2014, 10, 1)
            posts = self.profile_posts
            i= self.count_documents()
            try:
                for post in takewhile(lambda p: p.date_local > until, dropwhile(lambda p: p.date_local > now, posts)):
                    print("Extracting posts published yesterday" )
                    self.get_post_contents(post)
                if self.profile_posts.count > self.count_documents():
                    print("Getting remaining posts of previous extraction from ", self.account, ", checkpoint at ", since)
                    for post in takewhile(lambda p: p.date > old_date, dropwhile(lambda p: p.date > since, posts)):
                        print("Extracting post #", i ," published at ", post.date_local)
                        self.get_post_contents(post)
                        i += 1
            except Exception as e:
                LogSvs.add_error(type = type(e).__name__, traceback = traceback.format_exc(), function = "recurrent_extraction", account = self.account)
        else:
            return None
            
    def get_post_contents(self, post): # pragma: no cover
        self.post_id = str(uuid.uuid4())[2:-3]
        if post.comments > 0:
            self.get_post_comments(post)
            self.set_post_comments(post)
        self.get_post_media(post)
        self.download_post_images()
        self.set_post_dict(post)
        self.set_metadata()
        self.store_posts()
        self.send_post_kafka()

    def get_post_comments(self, post):  
            try:
                self.post_comments_list = ExtractSvs.extract_comment(postX = post)
                return self.post_comments_list
            except Exception as e:
                print(e)
                LogSvs.add_error(type = type(e).__name__, traceback = traceback.format_exc(), function = "get_post_comments", account = self.account)        

    def set_post_comments(self, post):
            try:
                self.comments_list = []
                if self.post_comments_list is not None: 
                    for comment in self.post_comments_list:
                        replies = ExtractSvs.extract_comment_reply(commentX = comment)
                        coment_dict = {
                            "Post ID": self.post_id,
                            "Comment": {
                                "Text": str(comment),
                                "Replies": replies
                            }
                        }
                        self.comments_list.append(coment_dict)
                        self.store_posts_comments(coment_dict)
                        self.send_comment_kafka(str(coment_dict))
                    return self.comments_list
            except IndexError as e:
                print(e)
                logging.debug(e, "API bug when retrieving comments for ", post)
            except Exception as e:
                print(e)
                LogSvs.add_error(type = type(e).__name__, traceback = traceback.format_exc(), function = "get_post_comments", account = self.account)        

    def get_post_media(self, post):
        image_list = []
        self.video_list = []
        cont_img = 0
        cont_video = 0
        try:
            post_type = post.typename
            if post_type == "GraphImage":
                image_list.append(post.url)
                cont_img = 1
            elif post_type == "GraphSidecar":
                post_slides = ExtractSvs.extract_post_slides(postX = post)
                for slide in post_slides:
                    if not slide.is_video:
                        cont_img += 1
                    else:
                        cont_video += 1
                    image_list.append(slide.display_url)
            elif post_type == "GraphVideo":
                video_info = {
                    "Video views": post.video_view_count,
                    "Video duration": post.video_duration
                }
                image_list.append(post.url)
                self.video_list.append(video_info)
                cont_video = 1

            self.dict_media = {
                'image_list': image_list,
                'cont_img': cont_img,
                'cont_video': cont_video
            }
            return self.dict_media
        except Exception as e:
            LogSvs.add_error(type = type(e).__name__, traceback = traceback.format_exc(), function = "get_post_media", account = self.account)
        
    def download_post_images(self):
        image_list = self.dict_media['image_list']
        download_list= []
        download_dict = {
            "id": str(self.post_id),
            "urls": image_list,
            "path": "./ig_photos/"+ self.account + "/" + str(self.post_id) 
        }
        print("paso1")
        download_list.append(download_dict)
        DowloadSvs.download(download_list)
        print("paso2")
        return download_dict
        
    def set_post_dict(self, post):
        try:
            new_caption = emoji.demojize(post.caption, delimiters=("", ""))
            self.post_dict = {
                "Profile": post.owner_username,
                "Local date": str(post.date_local).replace(",", ":"),
                "Post type": post.typename,
                "Number of comments": post.comments,
                "Likes": post.likes,
                "Caption": new_caption,
                "Hashtags used": post.caption_hashtags,
                "Tagged users": post.tagged_users,
                "Image links": self.dict_media['image_list'],
                "Video info": self.video_list
            }
            return self.post_dict
        except Exception as e:
            LogSvs.add_error(type = type(e).__name__, traceback = traceback.format_exc(), function = "set_post_dict", account = self.account)

    def set_metadata(self):
        istext = 0
        try:
            if self.post_dict['Caption'] is not None:
                istext = 1
            self.dict_metadata = {
                "uuid": self.post_id,
                "status": "raw",
                "query_date": datetime.now().strftime(producer.defines.DATE_FORMAT),
                "last_status_date": datetime.now().strftime(producer.defines.DATE_FORMAT),
                "social_network": "Instagram",
                "account": {
                    "query_value": self.account,
                    "query_description": self.profile.biography
                },
                "media_type": {
                    "text": istext,
                    "image": self.dict_media['cont_img'],
                    "video": self.dict_media['cont_video']
                },
                "images_path": "./Instagram-Producer/ig_photos/" + self.account + "/" + str(self.post_id),
                "raw_data": self.post_dict
            }
            return self.dict_metadata
        except Exception as e:
            LogSvs.add_error(type = type(e).__name__, traceback = traceback.format_exc(), function = "set_metadata", account = self.account)

    def store_posts(self):
        try:
            StorageSvs.store(producer.defines.COLLECTION_NAME, self.dict_metadata)
        except Exception as e:
            LogSvs.add_error(type = type(e).__name__, traceback = traceback.format_exc(), function = "store_pasts", account = self.account)
    
    def store_posts_comments(self, comment_dict):
        try:
            StorageSvs.store(producer.defines.COLLECTION_COMMENTS_NAME, comment_dict)
        except Exception as e:
            LogSvs.add_error(type = type(e).__name__, traceback = traceback.format_exc(), function = "store_posts_comments", account = self.account)
 
    def send_post_kafka(self):
        try:
            #lista_store = []
            #lista_store.append(self.dict_metadata)
            print("dict metadata")
            print(self.dict_metadata)
            StreamerSvs.publish(self.dict_metadata, producer.defines.POSTS_QUEUE)
        except Exception as e:
            LogSvs.add_error(type = type(e).__name__, traceback = traceback.format_exc(), function = "send_post_kafka", account = self.account)

    def send_comment_kafka(self, comment_dict):
        try:
            StreamerSvs.publish(comment_dict, producer.defines.COMMENTS_QUEUE)
        except Exception as e:
            print(e)
            LogSvs.add_error(type = type(e).__name__, traceback = traceback.format_exc(), function = "send_comment_kafka", account = self.account)

    def get_date_ranges(self):
        try:
            date_field = "raw_data.Local date"
            latest_post_date = StorageSvs.find_one(producer.defines.COLLECTION_NAME, {producer.defines.PROFILE_QUERY: self.account}, {date_field: 1}, [(date_field, -1)] )
            first_post_date = StorageSvs.find_one(producer.defines.COLLECTION_NAME, {producer.defines.PROFILE_QUERY: self.account},  {date_field: 1}, [(date_field, 1)] )
            if latest_post_date != None and first_post_date != None:
                return {"latest_post_date": datetime.strptime(latest_post_date['raw_data']['Local date'], producer.defines.DATE_FORMAT),"first_post_date": datetime.strptime(first_post_date['raw_data']['Local date'], producer.defines.DATE_FORMAT)}
            else:
                return None
        except Exception as e:
            LogSvs.add_error(type = type(e).__name__, traceback = traceback.format_exc(), function = "get_date_ranges", account = self.account)

    def count_documents(self): # pragma: no cover
        try:
            return StorageSvs.count(producer.defines.COLLECTION_NAME, {producer.defines.PROFILE_QUERY: self.account})
        except Exception as e:
            LogSvs.add_error(type = type(e).__name__, traceback = traceback.format_exc(), function = "count_docuemnts", account = self.account)

    def prepare_etl(self):
        self.set_conections()
        self.set_api()

    def initialize(self):
        self.prepare_etl()

if __name__ == '__main__': # pragma: no cover
    instance = InstagramExtract()
    instance.initialize()
    instance.start_etl()
    schedule.every().day.at("03:00").do(instance.start_etl)
    while True:
        schedule.run_pending()
        time.sleep(1)
