from producer.connection.kafka.streamer import IStreamer
#from Python.connection.kafka.streamer import IStreamer
from kafka import KafkaProducer
import json
from collections.abc import Iterable
class StreamerSvs(IStreamer):

    __producer = None

    def create_producer(kafka_host, kafka_port, encoding):
        domain = kafka_host + ':' + kafka_port
        StreamerSvs.__producer = KafkaProducer(
            bootstrap_servers = [domain], api_version = (0, 11, 5),
            value_serializer = lambda x: json.dumps(x).encode(encoding))
        
    def publish(info, topic):
        producer = StreamerSvs.__producer
        if isinstance(info, list):
            print("yes")
        if isinstance(info, list):
            for current_data in info:
                producer.send(topic, str(current_data)) 
        else:
            print("==========================================")
            print(str(info))
            producer.send(topic,str(info))
