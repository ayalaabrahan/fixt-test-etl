from abc import ABCMeta, abstractmethod

class IStreamer(metaclass=ABCMeta):

    @abstractmethod
    def create_producer(kafka_host, kafka_port, encoding):
        pass

    @abstractmethod
    def publish(info, topic):
        pass