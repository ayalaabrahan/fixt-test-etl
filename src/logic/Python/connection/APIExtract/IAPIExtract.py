from abc import ABCMeta, abstractmethod

class IAPIExtract(metaclass = ABCMeta):

    @abstractmethod
    def extract_post(*args, **kwargs):
        pass

    @abstractmethod
    def extract_comment(*args, **kwargs):
        pass

    @abstractmethod
    def extract_comment_reply(*args, **kwargs):
        pass

    @abstractmethod
    def extract_post_slides(*args, **kwargs):
        pass

    @abstractmethod
    def extract_profile(*args, **kwargs):
        pass

    @abstractmethod
    def login(*args, **kwargs):
        pass
    
    @abstractmethod
    def listener(*args, **kwargs):
        pass

    @abstractmethod
    def extract_videos_ids(*args, **kwargs):
        pass
    
    @abstractmethod
    def extract_videos_ids_by_date(*args, **kwargs):
        pass

    @abstractmethod
    def extract_videos_details(*args, **kwargs):
        pass