from typing import Iterable
from producer.connection.mongo.storage import IStorage
from pymongo import MongoClient
from datetime import datetime, timedelta

class StorageSvs(IStorage):

    __client = None
    __db = None
    __collections = {}
    NOW_TIME = " 00:00:00"

    def create_client(mongo_host, mongo_port):
        domain = mongo_host + ":" + mongo_port
        StorageSvs.__client = MongoClient(domain)

    def create_collections(db_name, collections):
        StorageSvs.__db = StorageSvs.__client[db_name]
        for collection in collections:
            new_collection = {collection: StorageSvs.__db[collection]}
            StorageSvs.__collections.update(new_collection)

    def store(collection_name, data):
        collection = StorageSvs.__collections[collection_name]
        if isinstance(data, list):
            collection.insert_many(data)
        else:
            collection.insert_one(data)

    def count(collection_name, query):
        collection = StorageSvs.__collections[collection_name]
        documents_quantity =  collection.count_documents(query)
        return documents_quantity

    def find_one(collection_name, query, order, sorted):
        collection = StorageSvs.__collections[collection_name]
        found_data = collection.find_one(query, order, sort = sorted)
        return found_data

    def exist_database(database_name):
        if StorageSvs.__client is None:
            return False
        else:
            for database in StorageSvs.__client.list_databases():
                if database['name'] == database_name:
                    return True
        return False

    def get_cursor(db_name, collection_name):
        StorageSvs.__db = StorageSvs.__client[db_name]
        date_now = datetime.now() - timedelta(hours = 24)
        date_now = str(date_now).split(' ')[0] + StorageSvs.NOW_TIME
        #cursor = StorageSvs.__db[collection_name].find({"query_date":{ "$gt": date_now}})
        cursor = StorageSvs.__db[collection_name].find()
        return cursor
    
    def delete_database():
        try:
            StorageSvs.__client.drop_database('Facebook')
            print('delete')
        except Exception as e:
            print('Not deleted', e)