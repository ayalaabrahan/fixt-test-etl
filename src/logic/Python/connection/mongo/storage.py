from abc import ABCMeta, abstractmethod

class IStorage(metaclass=ABCMeta):

    @abstractmethod
    def create_client(mongo_host, mongo_port):
        pass

    @abstractmethod
    def create_collections(db_name, collections):
        pass
        
    @abstractmethod
    def store(collection_name, data):
        pass

    @abstractmethod
    def count(collection_name, query):
        pass

    @abstractmethod
    def find_one(collection_name, query, order, sorted):
        pass
    
    @abstractmethod
    def exist_database(database_name):
        pass

    @abstractmethod
    def get_cursor(db_name, collection_name):
        pass