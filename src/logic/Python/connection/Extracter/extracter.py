from abc import ABCMeta, abstractmethod

class IExtracter(metaclass = ABCMeta):
    
    account = None
    
    def initialize(self):
        pass
    
    def set_conections(self):
        pass

    def set_kafka_conection(self):
        pass

    def set_mongo_conection(self):
        pass

    def set_mongo_collections(self):
        pass

    def set_API(self):
        pass