# ========================================= #
## ========= Importando librerias ======== ##
# ========================================= #
import producer.credentials as credentials
import producer.defines as defines
import schedule
from producer.connection.kafka.kafka_svs import StreamerSvs
from producer.connection.mongo.mongo_svs import StorageSvs
from producer.connection.Extracter.extracter import IExtracter
from producer.util.logs.log import LogSvs
import uuid
import concurrent.futures
from producer.tweets_listener import TweetsListener

from producer.APIExtract import ExtractSvs
import producer.extract_methods as em
import traceback
from time import sleep
from datetime import datetime, timedelta


class TwitterExtract(IExtracter):
    
    account = None

    def __init__(self, account, environment):
        try:
            self.account = account
            
            self.initialize() if environment == 'PRODUCTION' else None  
        except Exception as e:
            LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "__init__", account = self.account)

    def initialize(self):
        self.initialize_batch()

    def initialize_batch(self):
        try:
            self.set_connections()
            self.set_api()
            self.start_extraction()
        except Exception as e:
            LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "initialize_batch", account = self.account)

    def set_connections(self):
        try:
            self.set_kafka_conection()
            self.set_mongo_conection()
        except Exception as e:
            LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "set_connections", account = self.account)


    def set_kafka_conection(self):
        try:
            kafka_host = defines.KAFKA_HOST
            kafka_port = defines.KAFKA_PORT
            encoding = defines.ENCODE
            StreamerSvs.create_producer(kafka_host, kafka_port, encoding)
        except Exception as e:
            LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "set_kafka_conection", account = self.account)


    def set_mongo_conection(self):
        try:
            mongo_host = defines.MONGO_HOST
            mongo_port = defines.MONGO_PORT
            StorageSvs.create_client(mongo_host, mongo_port)
            self.set_mongo_collections()
        except Exception as e:
            LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "set_mongo_conection", account = self.account)


    def set_mongo_collections(self):
        try:    
            mongo_db_name = defines.MONGO_DATABASE_NAME
            mongo_collections_names = defines.MONGO_COLLECTIONS
            StorageSvs.create_collections(mongo_db_name, mongo_collections_names)
        except Exception as e:
            LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "set_mongo_collections", account = self.account)

    def set_api(self):
        self.set_tweepy_api()

    def set_tweepy_api(self):
        try:
            api_key = credentials.API_KEY
            api_secret_key = credentials.API_SECRET_KEY
            access_token = credentials.ACCESS_TOKEN
            access_token_secret = credentials.ACCESS_TOKEN_SECRET

            ExtractSvs.login(api_key = api_key,\
                api_secret_key = api_secret_key,\
                access_token = access_token,\
                access_token_secret = access_token_secret)
        except Exception as e:
            LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "set_tweepy_api", account = self.account)

    
    def start_extraction(self):
        try:
            account_username = self.account["username"]
            tweepy_cursor = ExtractSvs.extract_post(username = account_username)
            self.iterate_posts(tweepy_cursor)
            return tweepy_cursor   
        except Exception as e:
            LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "start_extraction", account = self.account)

                
    def iterate_posts(self, tweepy_cursor):   
        try:
            cont_posts = 0
            for status in tweepy_cursor.items():
                cont_posts += 1
                cont_media_photo = 0
                cont_media_video = 0
                tweet = status._json
                post_id = str(uuid.uuid4())
                if 'extended_entities' in tweet:
                    cont_media_photo, cont_media_video = self.get_media_posts(tweet, post_id)
                self.generate_metadata_post(tweet, post_id, cont_media_photo, cont_media_video)
            return cont_posts
        except Exception as e:
            LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "iterate_posts", account = self.account)


    def get_media_posts(self, tweet, post_id):
        try:
            cont_media_photo, cont_media_video = em.get_media_posts(tweet, post_id, self.account)
            return cont_media_photo, cont_media_video
        except Exception as e:
            LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "get_media_posts", account = self.account)


    def generate_metadata_post(self, tweet, post_id, cont_media_photo, cont_media_video):
        try: 
            tweet["created_at"] = em.transform_date(tweet["created_at"])
            tweet_dict = em.generate_metadata_post(tweet, post_id, cont_media_photo, cont_media_video)
            
            if "\"" in tweet["full_text"]:
                tweet["full_text"] = tweet["full_text"].replace("\"", "")

            if "\'" in tweet["full_text"]:
                tweet["full_text"] = tweet["full_text"].replace("\'", "")

            print(tweet_dict)
            self.store_data(tweet_dict)
            return tweet_dict
        except Exception as e:
            LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "generate_metadata_post", account = self.account)

        
    def store_data(self, tweet_dict):
        try:
            lista_store = []
            lista_store.append(tweet_dict)
            StorageSvs.store(defines.POSTS_COLLECTION_NAME, lista_store)
            StreamerSvs.publish(lista_store, defines.KAFKA_POSTS_TOPIC)
            return lista_store
        except Exception as e:
            LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "store_data", account = self.account)



def get_accounts(account_field):
    try:
        accounts_list = []
        for account in defines.ACCOUNTS:
            accounts_list.append(account[account_field])
        return accounts_list
    except Exception as e:
        LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "get_accounts")


def get_streaming():
    try:
        print("Inicia el streaming")
        list_ids = get_accounts("id")
        print("liste ids")
        streaming_api = TweetsListener(credentials.API_KEY, credentials.API_SECRET_KEY, credentials.ACCESS_TOKEN, credentials.ACCESS_TOKEN_SECRET)
        print("pase listeneer")
        streaming_api.filter(
            follow=list_ids
        )
        print("pase filter")
    except Exception as e:
        LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "get_streaming")


def start_batch(account):
    try:
        TwitterExtract(account, "PRODUCTION")
    except Exception as e:
        print('Error: No se puedo instanciar la clase', str(e))

def start_concurrent_batch_extractions():
    try:
        with concurrent.futures.ThreadPoolExecutor() as executor:
            executor.map(start_batch, defines.ACCOUNTS)
        print("Terminó el batch")
        #sleep(60)
        #get_streaming()
    except Exception as e:
        LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "start_concurrent_batch_extractions")

if __name__ == '__main__':
    start_concurrent_batch_extractions()
    """ Ejecución programada """
    schedule.every().day.at("20:30").do(start_concurrent_batch_extractions)

    while True:
        schedule.run_pending()
        sleep(1)


