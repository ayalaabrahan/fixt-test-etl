import producer.defines as defines
import tweepy
from time import sleep
import uuid
from http.client import IncompleteRead as http_incompleteRead
from urllib3.exceptions import IncompleteRead as urllib3_incompleteRead

from producer.connection.kafka.kafka_svs import StreamerSvs
from producer.connection.mongo.mongo_svs import StorageSvs
from producer.util.logs.log import LogSvs
import producer.extract_methods as em
import traceback

class TweetsListener(tweepy.Stream):

    account = None

    def store_data(self, tweet_dict):
        try:
            print("entro a store data")
            lista_store = []
            lista_store.append(tweet_dict)
            print("termino append")
            StorageSvs.store(defines.POSTS_COLLECTION_NAME, lista_store)
            print("termino store")
            StreamerSvs.publish(list(tweet_dict), defines.KAFKA_POSTS_TOPIC)
            print("termino publish")
            return lista_store
        except Exception as e:
            LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "store_data", account = self.account)


    def generate_metadata_post(self, tweet, post_id, cont_media_photo, cont_media_video):
        try:
            print("entro a metadata post")
            tweet["created_at"] = em.transform_date(tweet["created_at"])
            tweet_dict = em.generate_metadata_post(tweet, post_id, cont_media_photo, cont_media_video)
            #print(tweet_dict)
            if "\"" in tweet["full_text"]:
                tweet["full_text"] = tweet["full_text"].replace("\"", "")

            if "\'" in tweet["full_text"]:
                tweet["full_text"] = tweet["full_text"].replace("\'", "")

            self.store_data(tweet_dict)
            return tweet_dict
        except Exception as e:
            LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "generate_metadata_post", account = self.account)

    def iterate_post(self, status):
        try:
            print("entro a iterate post")
            cont_media_photo = 0
            cont_media_video = 0
            tweet = status._json
            post_id = str(uuid.uuid4())
            post_id = str(uuid.uuid4())
            if 'extended_entities' in tweet:
                cont_media_photo, cont_media_video = self.get_media_posts(tweet, post_id)
            self.generate_metadata_post(tweet, post_id, cont_media_photo, cont_media_video)
        except Exception as e:
            LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "iterate_post", account = self.account)

    def get_media_posts(self, tweet, post_id):
        try:
            print("entro a get media post")
            username = em.get_element_dict(tweet["user"]["screen_name"])
            cont_media_photo, cont_media_video = em.get_media_posts(tweet, post_id, username)
            return cont_media_photo, cont_media_video
        except Exception as e:
            LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "get_media_posts", account = self.account)

    def on_status(self, status):
        print("entro a on status")
        self.account = status._json["user"]["screen_name"]
        self.iterate_post(status)
        sleep(5)

    def on_limit(self, status):
        print("Rate limit exceeded, Sleep for 15 Mins")
        sleep(15*60)
        return True