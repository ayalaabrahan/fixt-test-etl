from producer.connection.APIExtract.IAPIExtract import IAPIExtract
import tweepy

class ExtractSvs(IAPIExtract):

    __auth = None
    __api = None

    def extract_post(*args, **kwargs):
        username = kwargs['username']
        return tweepy.Cursor(ExtractSvs.__api.user_timeline, screen_name = username, tweet_mode = "extended", include_rts = False)

    def login(*args, **kwargs):
        ExtractSvs.__auth = tweepy.OAuthHandler(kwargs['api_key'], kwargs['api_secret_key'])
        ExtractSvs.__auth.set_access_token(kwargs['access_token'], kwargs['access_token_secret'])
        ExtractSvs.__api = tweepy.API(ExtractSvs.__auth)