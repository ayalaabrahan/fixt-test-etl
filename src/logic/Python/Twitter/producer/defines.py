MONGO_DATABASE_NAME = 'Twitter'

MONGO_HOST = 'mongo'
MONGO_PORT = '27017'

POSTS_COLLECTION_NAME = 'twitter_raw'
POSTS_ERROR_COLLECTION_NAME = 'social_network_errors'


MONGO_COLLECTIONS = [POSTS_COLLECTION_NAME, POSTS_ERROR_COLLECTION_NAME]


KAFKA_HOST = 'kafka'
KAFKA_PORT = '9092'
KAFKA_POSTS_TOPIC = 'twitter_queue'

ENCODE = 'utf-8'

ACCOUNTS = [
             {'id':'1554871911917797382', 'username':'AnaliticaCaoba'}
            ]
DATE_FORMAT = '%Y-%m-%d %H:%M:%S'