from producer.util.downloads.download import DowloadSvs
import producer.defines as defines
from dateutil.parser import parse
from datetime import datetime, timedelta
import traceback
from producer.util.logs.log import LogSvs

def update_timezone(date):
    try:
        last_extracted_timestamp = str(parse(date))
        date_time_obj = datetime.strptime(last_extracted_timestamp, defines.DATE_FORMAT)
        hours = 5
        hours_added = timedelta(hours = hours)
        future_date_and_time = date_time_obj - hours_added
        return future_date_and_time
    except Exception as e:
        LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "update_timezone")

def transform_date(date_string):
    try:
        f0 = date_string.split(" +0000")
        f1 = f0[0]
        f2 =f0[1]
        date_transf = f1+f2
        date_time_obj = datetime.strptime(date_transf, "%a %b %d %H:%M:%S %Y")
        date_to_string = date_time_obj.strftime(defines.DATE_FORMAT)
        date_updated = update_timezone(date_to_string)
        final_date = date_updated.strftime(defines.DATE_FORMAT)
        return final_date
    except Exception as e:
        LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "transform_date")

def generate_metadata_post(tweet, post_id, cont_media_photo, cont_media_video):
    try:
        tweet_dict = {'uuid': post_id,
                'status': 'raw',
                'query_date': str(datetime.now().strftime(defines.DATE_FORMAT)),
                'last_status_date' : str(datetime.now().strftime(defines.DATE_FORMAT)),
                'social_network': "Twitter",
                'account':{"query_value": tweet["user"]["screen_name"],
                            "query_description": tweet["user"]["description"]},
                "media_type": {"text" :1,
                                "photo":cont_media_photo,
                                "video":cont_media_video},
                "images_path": "./twitter/data/" + tweet["user"]["screen_name"] + "/" + post_id,
                'raw_data': tweet
                }      
        return tweet_dict
    except Exception as e:
        LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "generate_metadata_post")

def get_media_posts(tweet, post_id, account):
    try:
        cont_media_photo = 0
        cont_media_video = 0
        for media in tweet['extended_entities']['media']:
            if media["type"] == 'photo':
                url_list = []
                url_list.append(media["media_url"])
                path = './Twitter/data/' + account["username"] + '/' + str(post_id)
                img_data = dict(id = post_id, urls = url_list, path = path)
                img_list = []
                img_list.append(img_data)
                #print(img_list)
                try:
                    #print(img_list)
                    DowloadSvs.download(img_list)
                except Exception as e:
                    print("Error in image download: ", e)
                cont_media_photo += 1
            if media["type"] == 'video' or media["type"] == 'animated_gif':
                cont_media_video += 1
        return cont_media_photo, cont_media_video
    except Exception as e:
        LogSvs.add_error(type = type(e)._name_, traceback = traceback.format_exc(), function = "get_media_posts")

def get_element_dict(username):
    for account in defines.ACCOUNTS:
        if account["username"] == username:
            return account