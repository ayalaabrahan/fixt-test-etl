import concurrent.futures
import urllib.request
import os

from producer.util.downloads.download_svs import IDowload

class DowloadSvs(IDowload):

  @classmethod
  def store(cls, data):
    try:
      url = data[0]
      file_path = str(data[1])
      file_name = str(data[2])
      if not os.path.exists(file_path):
        try:
          os.makedirs(file_path)
        except Exception as e:
          pass
      path = os.path.join(file_path, str(file_name + ".jpg"))
      urllib.request.urlretrieve(url, path)
    except Exception as e:
      print(e)

  def download(data, concurrent_thread = True):
    urls  = [(url, path['path'], str(path['id']) + '_' + str(index)) for path in data for  index, url in enumerate(path['urls'])]
    if concurrent_thread:
      with concurrent.futures.ThreadPoolExecutor() as executor:
        executor.map(DowloadSvs.store, urls)
    else: 
      map(DowloadSvs.store, urls)