from abc import ABCMeta, abstractmethod

class IDowload(metaclass=ABCMeta):
  
  @abstractmethod
  def download(data):
    pass