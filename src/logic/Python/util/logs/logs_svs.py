from abc import ABCMeta, abstractmethod

class Ilogs(metaclass=ABCMeta):

    @abstractmethod
    def add_error(**kwargs):
        pass