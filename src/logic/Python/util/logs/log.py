from producer.util.logs.logs_svs import Ilogs
from producer.connection.mongo.mongo_svs import StorageSvs


class LogSvs(Ilogs):
    def add_error(**kwargs):
        StorageSvs.store('social_network_errors', kwargs)